<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Bình Xiaomi Store</title>
<script src="Plugin/jquery-2.2.3.js"></script>
<link rel="stylesheet" type="text/css" href="Plugin/bootstrap-3.3.6-dist/css/bootstrap.css">
<script src="Plugin/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="Index.css">
<script src="Plugin/materialize/js/materialize.js"></script>
<script src="Plugin/jquery-ui-1.12.0-rc.2/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="Plugin/jquery-ui-1.12.0-rc.2/jquery-ui.css">
<script src="Scripts/Index.js"></script>
<script src="Plugin/w2ui/w2ui-1.4.3.js"></script>
<script src="./Plugin/adamwdraper-Numeral-js-7487acb/numeral.js"></script>
<link rel="stylesheet" type="text/css" href="Plugin/w2ui/w2ui-1.4.3.css">

<style>
#main-content{
	padding:0px 0px 0px;
	}
	
#toast-container {
  display: block;
  position: fixed;
  z-index: 10000;
}

@media only screen and (max-width: 600px) {
  #toast-container {
    min-width: 100%;
    bottom: 0%;
  }
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
  #toast-container {
    left: 5%;
    bottom: 7%;
    max-width: 90%;
  }
}

@media only screen and (min-width: 993px) {
  #toast-container {
    top: 10%;
    right: 7%;
    max-width: 86%;
  }
}

.toast {
  border-radius: 2px;
  top: 0;
  width: auto;
  clear: both;
  margin-top: 10px;
  position: relative;
  max-width: 100%;
  height: auto;
  min-height: 48px;
  line-height: 1.5em;
  word-break: break-all;
  background-color: #323232;
  padding: 10px 25px;
  font-size: 1.1rem;
  font-weight: 300;
  color: #fff;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-justify-content: space-between;
      -ms-flex-pack: justify;
          justify-content: space-between;
}

.toast .btn, .toast .btn-large, .toast .btn-flat {
  margin: 0;
  margin-left: 3rem;
}

.toast.rounded {
  border-radius: 24px;
}

@media only screen and (max-width: 600px) {
  .toast {
    width: 100%;
    border-radius: 0;
  }
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
  .toast {
    float: left;
  }
}

@media only screen and (min-width: 993px) {
  .toast {
    float: right;
  }
}

</style>

</head>


<script>
$(document).ready(function(e) {
    $(".nhasx").click(function(e) {
        var result = $(this).val();
		var arr = result.split(" ");
		var loaihanghoaid = arr[0];
		var nhasanxuatid = arr[1];
		$.post("ajax.php?mod=Hanghoa&act=DanhSach", 
		{
			loaihanghoaid:arr[0],
			nhasanxuatid:arr[1]
			}, function(data){
				$("#hanghoaContainer").html(data);
			});
    });
	$(".btn-block").click(function(e) {
        var result = $(this).val();
		$.post("xuly.php", 
		{
			loaiId:result,
			}, function(data){
				$("#hanghoaContainer").html(data);
			});
    });
	/*function openThemPopup(){

			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formThem)
			{
				$().w2form({
					name:'formThem',
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=nhasanxuat',
					fields: [
								{ field: 'tentaikhoan', type: 'text', required: true, html: { caption: 'Tên tài khoản', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'matkhau', type: 'password', required: true, html: { caption: 'Mật khẩu', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Thêm": function () { this.save({loai:'them'}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Thêm thành công<\/h5>", 3000);
										w2ui.nhasanxuatgrid.reload();
										}
										}
					  
					});
					
									
				}
			 $().w2popup('open', {
        title   : 'Thêm nhà sản xuất',
        body    : '<div id="formThem" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 200, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formThem.box).hide();
            event.onComplete = function () {
                $(w2ui.formThem.box).show();
                w2ui.formThem.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
                $('#w2ui-popup #formThem').w2render('formThem');
            }
        }
    });
			}
			$('#dangkibtn').click(function(e) {
                 openThemPopup();
            });*/
	
});
</script>
<?php

session_start();

?>
<!-- Modal Đăng nhập -->
<div id="loginModal" class="modal fade" role="dialog">
  <div class="modal-dialog">   
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-log-in"></span>Đăng nhập</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="POST" action='login_back.php'>
          <div class="form-group">
            <label for="username">Tên đăng nhập :</label>
            <input type="username" class="form-control" id="username" name="username">
          </div>
          <div class="form-group">
            <label for="password">Mật khẩu :</label>
            <input type="password" class="form-control" id="password" name="password">
          </div>
		     <input type="hidden" name="test" value="abc">  
          <div class="checkbox">
            <label>
              <input type="checkbox">
              Ghi nhớ đăng nhập</label>
          </div>
          <button type="submit" class="btn btn-default" name='ok'>Đăng nhập</button>
        </form>
      </div>
      <div class="modal-footer"><a>Quên mật khẩu?</a><a>Tạo tài khoản mới</a></div>
    </div>
  </div>
</div>

<!-- Modal Đăng ký tài khoản -->
<div id="signupModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span>Đăng ký tài khoản</h4>
      </div>
      <div class="modal-body">
        <h4>Thông tin tài khoản</h4>
        <form role="form">
          <div class="form-group">
            <label for="username">Tên đăng nhập :</label>
            <input type="username" class="form-control" id="username">
          </div>
          <div class="form-group">
            <label for="password">Mật khẩu :</label>
            <input type="password" class="form-control" id="password">
          </div>
          <div class="form-group">
            <label for="password1">Xác nhận mật khẩu :</label>
            <input type="password" class="form-control" id="password1">
          </div>
          <div class="form-group">
            <label for="email">Email :</label>
            <input type="email" class="form-control" id="email">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <h4>Thông tin khách hàng</h4>
        <form role="form">
          <div class="form-group">
            <label for="hoVaTen">Họ và Tên :</label>
            <input type="text" class="form-control" id="hoVaTen">
          </div>
          <div class="form-group">
            <label for="soDienthoai">Số điện thoại :</label>
            <input type="text" class="form-control" id="soDienthoai">
          </div>
          <div class="form-group">
            <label for="diaChi">Địa chỉ :</label>
            <input type="text" class="form-control" id="diaChi">
          </div>
          <br>
          <button type="submit" class="btn btn-default">Xác nhận</button>
        </form>
      </div>
    </div>
  </div>
</div>


<body>
<nav class="navbar navbar-fixed-top navbar-default">
  <div class="container-fluid">
    <div class="navbar-header"> <a class="navbar-brand" href="#" title=""><img src="Resource/HinhAnh/logo/2000px-Xiaomi_logo.svg.png" width="50" height="50" alt=""/>Bình Xiaomi Store</a> </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="Index.php?mod=Hanghoa&act=KiemtraDonhang" ><span class="glyphicon glyphicon-list-alt"></span> Kiểm tra đơn hàng</a></li>
      <li><a href="Index.php?mod=Hanghoa&act=ChiTietGioHang"><span class="glyphicon glyphicon-shopping-cart"></span> Giỏ hàng</a></li>
       <li class="dangky"><a href="#" data-toggle="modal" data-target="#signupModal"><span class="glyphicon glyphicon-user"></span> Đăng ký tài khoản</a></li>
      <li class="dangnhap"><a href="#" data-toggle="modal" data-target="#loginModal" ><span class="glyphicon glyphicon-log-in"></span> Đăng nhập</a></li>
	  <?php
	if (isset($_SESSION['success']) && $_SESSION['success']!=null)
	{
		$a=$_SESSION['username'];
		echo '<script>$(".dangky").remove();
		$(".dangnhap").remove();
		</script>';
		echo '<li><a href="#"></span> Xin chào '.$a.'</a></li>';
		echo '<li><a href="destroy_session_client.php" ><span class="glyphicon glyphicon-log-out"></span>Thoát</a></li>';
	}
?>
 
    </ul>
    <ul class="nav navbar-nav navbar-left">
      <li><a href="Index.php">Trang chủ</a></li>
      <li><a href="#">Giới thiệu</a></li>
      <li><a href="#">Liên lạc</a></li>
    </ul>
  </div>
</nav>
<div class="header">
  <div class="container-fluid"> <img src="Resource/HinhAnh/logo/2000px-Xiaomi_logo.svg.png" class="img-responsive" width="215" height="215"/> <span class="tieude"> Pin sạc dự phòng - Tai nghe - Loa bluetooth - Phụ kiện </span>
    <hr>
    <span class="welcome"> Chào mừng các bạn đã đến với shop </span> </div>
</div>
<div class="body"> <br>
  <div class="row">
    <div class="col-md-2 sideleft khung">
      <h1>Mục lục</h1>
      <hr class="hrMucLuc">  
     <div class="list-group">
  <?php
 include_once("./MVC/Model/NhaSanxuat.php");
 $nhasanxuatdb = new NhaSanxuat();
 $listnhasanxuat = $nhasanxuatdb->getlistnhasanxuat();
 foreach($listnhasanxuat as $nhasanxuat){
	 $chuoi = <<<EOD
	  <a href="Index.php?mod=ajax&act=DanhSach&nhasanxuatid={$nhasanxuat['nhasanxuatid']}&loai=getbynhasanxuat" class="list-group-item">{$nhasanxuat['tennhasanxuat']}</a> 
EOD;
	echo $chuoi;
 }
  ?>
</div>
    </div>
    <div class="col-md-9 khung" style="margin:0px auto">
      <h1><?php
	  if(isset($_GET['act']))
	  {
      if($_GET['act'] == 'ChiTietGioHang')
	  	echo "Chi tiết giỏ hàng";
	  else if($_GET['act'] == 'ChiTietHangHoa')
	  	echo "Chi tiết hàng hóa";
	  else if($_GET['act'] == 'KiemtraDonhang')
	  	echo "Kiểm tra đơn hàng";
	 else
	 	echo "Sản phẩm";
	  }
	  else echo "Sản phẩm";
	  
	  ?></h1>
      <hr>
      <div class="container-fluid" id="hanghoaContainer">
      <?php
 if(isset($_GET["mod"]))
            {
                include_once("MVC/Controller/".$_GET['mod']."/".$_GET['act'].'.php');
            }
            else
            {
                include_once("MVC/Controller/ajax/DanhSach.php");
				
            }
			?>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="pagination page-nav" id="paginateContainer">
          
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<footer>
  <div class="container-fluid footer">
    <h4>
    Copyright by BinhProduction
    </h4>
  </div>
</footer>
</html>