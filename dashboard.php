<?php
	session_start();
	if (!(isset($_SESSION['userid']) && $_SESSION['userid'] != '')) {
		header ("Location: login.php");
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Dashboard - Hệ thống bán lẻ</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="Plugin/Template/flaty/assets/bootstrap/css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="Plugin/font-awesome-4.6.3/css/font-awesome.css">
	<link href="Plugin/Template/flaty/css/flaty.css" rel="stylesheet"/>
	<link href="Plugin/Template/flaty/css/flaty-responsive.css" rel="stylesheet"/>
	<script type="text/javascript" src="Plugin/Template/flaty/assets/jquery/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="Plugin/jquery-ui-1.12.0-rc.2/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="Plugin/jquery-ui-1.12.0-rc.2/jquery-ui.structure.css">
   	<link rel="stylesheet" type="text/css" href="Plugin/jquery-ui-1.12.0-rc.2/jquery-ui.theme.css">
    <script src="Plugin/Template/flaty/assets/jquery-slimscroll/jquery.slimscroll.js"></script>
	<script src="Plugin/Template/flaty/assets/jquery-cookie/jquery.cookie.js"></script>
	<script type="text/javascript" src="Plugin/Template/flaty/assets/bootstrap/js/bootstrap.js"></script>
	<script src="Plugin/Template/flaty/js/flaty.js"></script>
	<script type="text/javascript" src="Plugin/w2ui/w2ui-1.4.3.js"></script>
	<link rel="stylesheet" type="text/css" href="Plugin/w2ui/w2ui-1.4.3.css">
    <script type="text/javascript" src="Plugin/materialize/js/materialize.js"></script>
    <script src="Plugin/adamwdraper-Numeral-js-7487acb/numeral.js"></script>
    <script type="text/javascript">
        $("#btn-toggle-navbar").click(function () {
            $nav = $("#nav-horizontal");
            if ($nav.hasClass("collapse")) {
                $nav.removeClass("collapse").addClass("collapsed");
                $nav.css("height", "auto");
            } else {
                $nav.removeClass("collapsed").addClass("collapse");
                $nav.css("height", "");
            }
        });
    </script>
    
    <!-- bo nhung tag script include plugin vào đây khi muốn sử dụng them -->
    <script src="Plugin/Template/flaty/assets/code-prettify/code-prettify.js"></script> 
    <!-- bo nhung tag link, style vao day -->
    <link href="Plugin/Template/flaty/assets/code-prettify/code-prettify.css" rel="stylesheet" />
<style>
#main-content{
	padding:0px 0px 0px;
	}
	
#toast-container {
  display: block;
  position: fixed;
  z-index: 10000;
}

@media only screen and (max-width: 600px) {
  #toast-container {
    min-width: 100%;
    bottom: 0%;
  }
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
  #toast-container {
    left: 5%;
    bottom: 7%;
    max-width: 90%;
  }
}

@media only screen and (min-width: 993px) {
  #toast-container {
    top: 10%;
    right: 7%;
    max-width: 86%;
  }
}

.toast {
  border-radius: 2px;
  top: 0;
  width: auto;
  clear: both;
  margin-top: 10px;
  position: relative;
  max-width: 100%;
  height: auto;
  min-height: 48px;
  line-height: 1.5em;
  word-break: break-all;
  background-color: #323232;
  padding: 10px 25px;
  font-size: 1.1rem;
  font-weight: 300;
  color: #fff;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-align-items: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-justify-content: space-between;
      -ms-flex-pack: justify;
          justify-content: space-between;
}

.toast .btn, .toast .btn-large, .toast .btn-flat {
  margin: 0;
  margin-left: 3rem;
}

.toast.rounded {
  border-radius: 24px;
}

@media only screen and (max-width: 600px) {
  .toast {
    width: 100%;
    border-radius: 0;
  }
}

@media only screen and (min-width: 601px) and (max-width: 992px) {
  .toast {
    float: left;
  }
}

@media only screen and (min-width: 993px) {
  .toast {
    float: right;
  }
}

</style>
</head>
<body>
    




<div class="skin-black">
    <!-- BEGIN Navbar -->
    <div id="navbar" class="navbar">
        <button type="button" class="navbar-toggle navbar-btn for-nav-horizontal collapsed" id="btn-toggle-navbar">
            <span class="fa fa-bars"></span>
        </button>
        <a class="navbar-brand" href="#">
            <small>
                <i class="fa fa-desktop"></i>
                Hệ thống bán lẻ
            </small>
        </a>
		<a class="nav navbar-nav navbar-right" href='../doanweb/destroy_session.php'>
	
				<i class="glyphicon glyphicon-log-in"></i>
				Thoát
		</a>
        <!-- END Navbar Menu -->
        
    </div>
    <!-- BEGIN Container -->
<div class="container" id="main-container">
<!-- BEGIN Sidebar -->
<div id="sidebar" class="navbar-collapse collapse">
<!-- BEGIN Navlist -->
<ul class="nav nav-list">
<!-- BEGIN Search Form -->
<li>
    <form target="#" method="GET" class="search-form">
        <span class="search-pan">
                            <button type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                            <input type="text" name="search" placeholder="Search ..." autocomplete="off" />
                        </span>
    </form>
</li>
<!-- END Search Form -->
<li>
    <a href="/RetailPOS/SaleManagement/Pos" target="_blank">
        <i class="fa fa-desktop"></i>
        <span><strong>BÁN HÀNG</strong></span>
    </a>
</li>
<li>
    <a href="/">
        <i class="fa fa-dashboard"></i>
        <span>Dashboard</span>
    </a>
</li>

<li>
    <a href="#" class="dropdown-toggle">
        <i class="fa fa-shopping-cart"></i>
        <span>Đơn hàng</span>
        <b class="arrow fa fa-angle-right"></b>
    </a>
    <!-- BEGIN Submenu -->
    <ul class="submenu" style="display: none;">
        <li>
            <a href="dashboard.php?mod=Admin&act=DonHangChuaXuly" class="dropdown-toggle">
                <i class="glyphicon glyphicon-log-out"></i>
                <span>Đơn hàng cần xử lý</span>
                <b class="arrow fa fa-angle-right"></b>
            </a>   
        </li>

      
    </ul>

    <!-- END Submenu -->
</li>



<li>
    <a href="#" class="dropdown-toggle">
        <i class="fa fa-truck"></i>
        <span>Giao hàng</span>
        <b class="arrow fa fa-angle-right"></b>
    </a>
    <!-- BEGIN Submenu -->
    <ul class="submenu" style="display: none;">
        <li><a href="dashboard.php?mod=Admin&act=PhieuGiaohangChuaXuly">Phiếu giao hàng cần xử lý</a></li>
        <li><a href="dashboard.php?mod=Admin&act=PhieuGiaohangCanGiao">Đơn hàng cần giao</a></li>
    </ul>
    <!-- END Submenu -->
</li>



<li>
    <a href="#" class="dropdown-toggle">
        <i class="glyphicon glyphicon-shopping-cart"></i>
        <span>Hàng hóa</span>
        <b class="arrow fa fa-angle-right"></b>
    </a>
    <!-- BEGIN Submenu -->
    <ul class="submenu" style="display: none;">
        <li><a href="dashboard.php?mod=Admin&act=LoaiHanghoa">Nhóm hàng hóa</a></li>
        <li><a href="dashboard.php?mod=Admin&act=NhaSanxuat">Nhà sản xuất</a></li>
        <li><a href="dashboard.php?mod=Admin&act=ThuocTinh">Thuộc tính</a></li>
        <li><a href="dashboard.php?mod=Admin&act=MotaThuoctinh">Mô tả thuộc tính</a></li>
        <li><a href="dashboard.php?mod=Admin&act=Hanghoa">Hàng hóa</a></li>
    </ul>
    <!-- END Submenu -->
</li>
</ul>
<!-- END Navlist -->
<!-- BEGIN Sidebar Collapse Button -->
<div id="sidebar-collapse" class="visible-lg">
    <i class="fa fa-angle-double-left"></i>
</div>
<!-- END Sidebar Collapse Button -->
</div>
<!-- END Sidebar -->
<!-- BEGIN Content -->
<div id="main-content" class="full-width">
    <?php
 if(isset($_GET["mod"]))
            {
                include_once("MVC/Controller/".$_GET['mod']."/".$_GET['act'].'.php');
            }
            else
            {
                include_once("MVC/Controller/Admin/LoaiHanghoa.php");
				
            }
			?>
</div>
</div>