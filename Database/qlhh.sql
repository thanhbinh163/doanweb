-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2016 at 09:26 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qlhh`
--

-- --------------------------------------------------------

--
-- Table structure for table `chitietdonhang`
--

CREATE TABLE `chitietdonhang` (
  `chitietdonhangid` int(11) NOT NULL,
  `donhangid` int(11) NOT NULL,
  `hanghoaid` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `giatien` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chitietdonhang`
--

INSERT INTO `chitietdonhang` (`chitietdonhangid`, `donhangid`, `hanghoaid`, `soluong`, `giatien`) VALUES
(1, 1, 3, 1, 70000),
(2, 1, 20, 1, 120000),
(16, 25, 2, 2, 10000000),
(17, 26, 1, 1, 13345000),
(18, 27, 2, 1, 10000000),
(23, 34, 2, 1, 10000000),
(24, 34, 7, 1, 16000000);

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

CREATE TABLE `donhang` (
  `donhangid` int(11) NOT NULL,
  `madonhang` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngaylap` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tongtien` int(255) NOT NULL,
  `trangthai` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`donhangid`, `madonhang`, `ngaylap`, `tongtien`, `trangthai`) VALUES
(1, 'DH00001', '2016-06-09 00:00:00', 190000, 3),
(2, 'DH00002', '2016-06-09 00:00:00', 90000, 2),
(25, 'DH25', '2016-06-21 01:10:03', 20000000, 3),
(26, 'DH26', '2016-06-21 02:13:16', 13345000, 2),
(27, 'DH27', '2016-06-22 19:35:06', 10000000, 2),
(34, 'DH34', '2016-06-23 02:04:42', 26000000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hanghoa`
--

CREATE TABLE `hanghoa` (
  `hanghoaid` int(11) NOT NULL,
  `mahanghoa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tenhanghoa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `giaban` int(11) DEFAULT NULL,
  `namsanxuat` int(11) DEFAULT NULL,
  `moihaycu` tinyint(1) DEFAULT NULL,
  `hinh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hinh2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hinh3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hinh4` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mota` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nhasanxuatid` int(11) DEFAULT NULL,
  `loaihanghoaid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hanghoa`
--

INSERT INTO `hanghoa` (`hanghoaid`, `mahanghoa`, `tenhanghoa`, `giaban`, `namsanxuat`, `moihaycu`, `hinh`, `hinh2`, `hinh3`, `hinh4`, `mota`, `nhasanxuatid`, `loaihanghoaid`) VALUES
(1, 'HTC00001', 'HTC Desire 620G', 13345000, 2014, 1, 'HTC01.png', 'HTC01NGANG.jpg', 'HTC01NAM.jpg', 'HTC01SAU.jpg', '', 1, 1),
(2, 'HTC00002', 'HTC Butterfly S', 10000000, 2013, 0, 'HTC02.png', 'HTC02NGANG.jpg', 'HTC02NAM.jpg', 'HTC02SAU.jpg', NULL, 1, 1),
(3, 'HTC00003', 'HTC Nexus 9 Volantis ', 12000000, 2014, 1, 'HTC03.png', 'HTC04NGANG.jpg', 'HTC04NAM.jpg', 'HTC04SAU.jpg', NULL, 1, 1),
(4, 'HTC00004', 'HTC One 32GB', 13500000, 2014, 1, 'HTC04.jpg', 'HTC04NGANG.jpg', 'HTC04NAM.jpg', 'HTC04SAU.jpg', NULL, 1, 1),
(5, 'HTC00005', 'HTC Butterfly', 9990000, 2012, 0, 'HTC05.jpg', 'HTC05NGANG.jpg', 'HTC05NAM.jpg', 'HTC05SAU.jpg', NULL, 1, 1),
(6, 'IPHONE00001', 'iPhone 5C 16GB', 10000000, 2012, 0, 'IPHONE01.png', 'IPHONE01NGANG.jpg', 'IPHONE01NAM.jpg', 'IPHONE01SAU.jpg', NULL, 2, 1),
(7, 'IPHONE00002', 'iPhone 6 16GB', 16000000, 2014, 1, 'IPHONE02.png', 'IPHONE02NGANG.jpg', 'IPHONE02NAM.jpg', 'IPHONE02SAU.jpg', NULL, 2, 1),
(8, 'IPHONE00003', 'iPhone 3GS 8GB', 10600000, 2013, 0, 'IPHONE03.png', 'IPHONE03NGANG.jpg', 'IPHONE03NAM.jpg', 'IPHONE03SAU.jpg', NULL, 2, 1),
(9, 'IPHONE00004', 'iPhone 5S 16GB', 12000000, 2013, 0, 'IPHONE04.jpg', 'IPHONE04NGANG.jpg', 'IPHONE04NAM.jpg', 'IPHONE04SAU.jpg', NULL, 2, 1),
(10, 'IPHONE00005', 'iPhone 5C 16GB', 11000000, 2014, 1, 'IPHONE05.png', 'IPHONE05NGANG.jpg', 'IPHONE05NAM.jpg', 'IPHONE05SAU.jpg', NULL, 2, 1),
(11, 'NOKIA00001', 'Microsoft Lumia 535', 3000000, 2011, 0, 'NOKIA01.png', 'NOKIA01NGANG.jpg', 'NOKIA01NAM.jpg', 'NOKIA01SAU.jpg', NULL, 3, 1),
(12, 'NOKIA00002', 'Nokia Lumia 930', 4500000, 2013, 1, 'NOKIA02.png', 'NOKIA02NGANG.jpg', 'NOKIA02NAM.jpg', 'NOKIA02SAU.jpg', NULL, 3, 1),
(13, 'NOKIA00003', 'Nokia X+', 5000000, 2014, 1, 'NOKIA03.png', 'NOKIA03NGANG.jpg', 'NOKIA03NAM.jpg', 'NOKIA03SAU.jpg', NULL, 3, 1),
(14, 'NOKIA00004', 'Nokia N1', 12000000, 2014, 1, 'NOKIA04.png', 'NOKIA04NGANG.jpg', 'NOKIA04NAM.jpg', 'NOKIA04SAU.jpg', NULL, 3, 1),
(15, 'NOKIA00005', 'Nokia 220', 1800000, 2011, 0, 'NOKIA05.png', 'NOKIA05NGANG.jpg', 'NOKIA05NAM.jpg', 'NOKIA05SAU.jpg', NULL, 3, 1),
(16, 'SAMSUNG00001', 'Samsung Galaxy Note 4', 13000000, 2014, 1, 'SAMSUNG01.png', 'SAMSUNG01NGANG.jpg', 'SAMSUNG01NAM.jpg', 'SAMSUNG01SAU.jpg', NULL, 4, 1),
(17, 'SAMSUNG00002', 'Samsung Galaxy Tab 3 ', 10000000, 2013, 0, 'SAMSUNG02.png', 'SAMSUNG02NGANG.jpg', 'SAMSUNG02NAM.jpg', 'SAMSUNG02SAU.jpg', NULL, 4, 2),
(18, 'SAMSUNG00003', 'Samsung Galaxy Tab 3 8" 3G 16Gb (T311)', 11300000, 2011, 0, 'SAMSUNG03.jpg', 'SAMSUNG03NGANG.jpg', 'SAMSUNG03NAM.jpg', 'SAMSUNG03SAU.jpg', NULL, 4, 2),
(19, 'SAMSUNG00004', 'Samsung Galaxy Tab 4 7.0 (SM-T231)', 10000000, 2013, 1, 'SAMSUNG04.png', 'SAMSUNG04NGANG.jpg', 'SAMSUNG04NAM.jpg', 'SAMSUNG04SAU.jpg', NULL, 4, 2),
(20, 'SAMSUNG00005', 'Samsung Galaxy Note 3 Neo', 9000000, 2013, 1, 'SAMSUNG05.png', 'SAMSUNG05NGANG.jpg', 'SAMSUNG05NAM.jpg', 'SAMSUNG05AU.jpg', NULL, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `loaihanghoa`
--

CREATE TABLE `loaihanghoa` (
  `loaihanghoaid` int(11) NOT NULL,
  `maloaihanghoa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tenloaihanghoa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loaihanghoa`
--

INSERT INTO `loaihanghoa` (`loaihanghoaid`, `maloaihanghoa`, `tenloaihanghoa`, `icon`) VALUES
(1, 'DT', 'Điện thoại', 'LHH_1.png'),
(2, 'MTB', 'Máy tính bảng', 'LHH_2.png'),
(3, 'SACDP', 'Sạc dự phòng', 'LHH_3.png'),
(4, 'PHUKIEN', 'Phụ kiện', 'LHH_4.png');

-- --------------------------------------------------------

--
-- Table structure for table `motathuoctinh`
--

CREATE TABLE `motathuoctinh` (
  `motathuoctinhid` int(11) NOT NULL,
  `thuoctinhid` int(11) NOT NULL,
  `hanghoaid` int(11) NOT NULL,
  `mota` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `motathuoctinh`
--

INSERT INTO `motathuoctinh` (`motathuoctinhid`, `thuoctinhid`, `hanghoaid`, `mota`) VALUES
(1, 7, 1, 'Super LCD 2, 5", HD'),
(2, 6, 1, 'Android 4.4 (KitKat)'),
(3, 1, 1, 'MT6592 8 nhân 32-bit, 1.7 GHz'),
(4, 3, 1, 'sau 8 MP, trước 5 MP'),
(5, 2, 1, '1 GB'),
(6, 8, 1, '8 GB'),
(7, 4, 1, '2100 mAh'),
(8, 5, 1, 'Dài 150.1 mm - Rộng 72.7 mm - Dày 9.6 mm');

-- --------------------------------------------------------

--
-- Table structure for table `nhasanxuat`
--

CREATE TABLE `nhasanxuat` (
  `nhasanxuatid` int(11) NOT NULL,
  `manhasanxuat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tennhasanxuat` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nhasanxuat`
--

INSERT INTO `nhasanxuat` (`nhasanxuatid`, `manhasanxuat`, `tennhasanxuat`) VALUES
(1, 'HTC', 'HTC'),
(2, 'IP', 'IPHONE'),
(3, 'NKI', 'NOKIA'),
(4, 'SSU', 'SAMSUNG');

-- --------------------------------------------------------

--
-- Table structure for table `phieugiaohang`
--

CREATE TABLE `phieugiaohang` (
  `phieugiaohangid` int(11) NOT NULL,
  `donhangid` int(11) NOT NULL,
  `tennguoinhan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sodienthoai` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phigiaohang` int(11) DEFAULT NULL,
  `xuly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phieugiaohang`
--

INSERT INTO `phieugiaohang` (`phieugiaohangid`, `donhangid`, `tennguoinhan`, `sodienthoai`, `diachi`, `phigiaohang`, `xuly`) VALUES
(1, 1, 'Hồ Thanh Bình', '0937179469', '41 Trần văn hoàng, P.9, Q.Tân Bình, TP.HCM', 500000, 1),
(3, 25, 'Hồ Thanh Binh', '937179469', '41 trần hưng đạo', 50000, 1),
(4, 26, '123', '123', '123', 20000, 1),
(5, 27, 'binh123', '123123123', '123', 12563322, 1),
(12, 34, 'binh', '0968382172', 'tân bình', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `thuoctinh`
--

CREATE TABLE `thuoctinh` (
  `thuoctinhid` int(11) NOT NULL,
  `tenthuoctinh` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thuoctinh`
--

INSERT INTO `thuoctinh` (`thuoctinhid`, `tenthuoctinh`) VALUES
(1, 'CPU'),
(2, 'RAM'),
(3, 'Camera'),
(4, 'Pin'),
(5, 'Kích thước'),
(6, 'Hệ điều hành'),
(7, 'Màn hình'),
(8, 'Bộ nhớ trong');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `username`, `password`) VALUES
(1, 'abc', '123'),
(2, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  ADD PRIMARY KEY (`chitietdonhangid`),
  ADD KEY `donhangid` (`donhangid`,`hanghoaid`),
  ADD KEY `hanghoaid` (`hanghoaid`);

--
-- Indexes for table `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`donhangid`);

--
-- Indexes for table `hanghoa`
--
ALTER TABLE `hanghoa`
  ADD PRIMARY KEY (`hanghoaid`),
  ADD KEY `nhasanxuatid` (`nhasanxuatid`),
  ADD KEY `loaihanghoaid` (`loaihanghoaid`);

--
-- Indexes for table `loaihanghoa`
--
ALTER TABLE `loaihanghoa`
  ADD PRIMARY KEY (`loaihanghoaid`);

--
-- Indexes for table `motathuoctinh`
--
ALTER TABLE `motathuoctinh`
  ADD PRIMARY KEY (`motathuoctinhid`),
  ADD KEY `thuoctinhid` (`thuoctinhid`),
  ADD KEY `thuoctinhid_2` (`thuoctinhid`,`hanghoaid`),
  ADD KEY `hanghoaid` (`hanghoaid`);

--
-- Indexes for table `nhasanxuat`
--
ALTER TABLE `nhasanxuat`
  ADD PRIMARY KEY (`nhasanxuatid`);

--
-- Indexes for table `phieugiaohang`
--
ALTER TABLE `phieugiaohang`
  ADD PRIMARY KEY (`phieugiaohangid`),
  ADD KEY `donhangid` (`donhangid`);

--
-- Indexes for table `thuoctinh`
--
ALTER TABLE `thuoctinh`
  ADD PRIMARY KEY (`thuoctinhid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  MODIFY `chitietdonhangid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `donhang`
--
ALTER TABLE `donhang`
  MODIFY `donhangid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `hanghoa`
--
ALTER TABLE `hanghoa`
  MODIFY `hanghoaid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `loaihanghoa`
--
ALTER TABLE `loaihanghoa`
  MODIFY `loaihanghoaid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `motathuoctinh`
--
ALTER TABLE `motathuoctinh`
  MODIFY `motathuoctinhid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `nhasanxuat`
--
ALTER TABLE `nhasanxuat`
  MODIFY `nhasanxuatid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `phieugiaohang`
--
ALTER TABLE `phieugiaohang`
  MODIFY `phieugiaohangid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `thuoctinh`
--
ALTER TABLE `thuoctinh`
  MODIFY `thuoctinhid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  ADD CONSTRAINT `chitietdonhang_ibfk_1` FOREIGN KEY (`donhangid`) REFERENCES `donhang` (`donhangid`),
  ADD CONSTRAINT `chitietdonhang_ibfk_2` FOREIGN KEY (`hanghoaid`) REFERENCES `hanghoa` (`hanghoaid`);

--
-- Constraints for table `hanghoa`
--
ALTER TABLE `hanghoa`
  ADD CONSTRAINT `hanghoa_ibfk_1` FOREIGN KEY (`nhasanxuatid`) REFERENCES `nhasanxuat` (`nhasanxuatid`),
  ADD CONSTRAINT `hanghoa_ibfk_2` FOREIGN KEY (`loaihanghoaid`) REFERENCES `loaihanghoa` (`loaihanghoaid`);

--
-- Constraints for table `motathuoctinh`
--
ALTER TABLE `motathuoctinh`
  ADD CONSTRAINT `motathuoctinh_ibfk_1` FOREIGN KEY (`thuoctinhid`) REFERENCES `thuoctinh` (`thuoctinhid`),
  ADD CONSTRAINT `motathuoctinh_ibfk_2` FOREIGN KEY (`hanghoaid`) REFERENCES `hanghoa` (`hanghoaid`);

--
-- Constraints for table `phieugiaohang`
--
ALTER TABLE `phieugiaohang`
  ADD CONSTRAINT `phieugiaohang_ibfk_1` FOREIGN KEY (`donhangid`) REFERENCES `donhang` (`donhangid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
