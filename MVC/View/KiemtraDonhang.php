<script>
$(document).ready(function(e) {
    $('#kiemtradonhangbtn').click(function(e) {
        $.post('ajax.php?act=donhang',
		{
			'madonhang':$('#madonhang').val(),
			'cmd':'getdonhangbymadonhang'
			},
		function(data){
			$('#container').html(data);
			});
    });
});

</script>

<div class="row">
  <div class="col-xs-4 col-xs-offset-4">
    <div>
      <label for="madonhang">Nhập mã đơn hàng</label>
    </div>
    <div style="display: inline-flex;">
      <input class="form-control" id="madonhang" type="text">
      <button id='kiemtradonhangbtn' style="padding-left: 11px; padding-top: 8px; margin-top: -6px; margin-left: 14px;" class="btn btn-info"><span style="font-size: x-large;" class="glyphicon glyphicon-search"></span></button>
    </div>
  </div>
</div>
<div id='container'></div>
