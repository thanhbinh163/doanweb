<link rel="stylesheet" type="text/css" href="Plugin/w2ui/w2ui-1.4.3.css">

<script>
$(document).ready(function(e) {
    $(".soluong").change(function(e) {
        var giaban = numeral().unformat($(this).parent().siblings('.giaban').text()); // tim trong anh em co cha la the <tr> dc chon xem co class .giaban k ?
		var soluong =$(this).val(); 
		var thanhtien = $(this).parent().siblings().children('.thanhtien');
		var hanghoaid = $(this).parent().siblings('.tenhanghoa').attr('hanghoaid');
		thanhtien.text(numeral(giaban*soluong).format('0,0'));
    	$.post('ajax.php?act=giohang',{
			'loai':'sua',
			'hanghoaid':hanghoaid,
			'soluong':soluong
			});
    });
	$('.btnXoa').click(function(e) {
		var self = this;
		var tenhanghoa = $(self).parent().siblings('.tenhanghoa').text();
		var hanghoaid = $(self).parent().siblings('.tenhanghoa').attr('hanghoaid');
		w2confirm('Bạn có chắc chắn muốn xóa <strong>'+ tenhanghoa +'<\/strong> ra khỏi giỏ hàng ?')
		.yes(function () { 
			$(self).parent().siblings().remove();
			$(self).parent().remove();
			$.post('ajax.php?act=giohang',{
			'loai':'xoa',
			'hanghoaid':hanghoaid,
			}, function(){Materialize.toast('Đã xóa '+ tenhanghoa +' ra khỏi giỏ hàng', 2000);});
			
			
		})
		.no(function () { 
		
		});
    });
	function openSuaPopup(data){
			//nếu chưa có formThem thì tạo mới
			if(!w2ui.form)
			{
				 
				$().w2form({
					name:'form',
					header:'Thông tin người nhận',
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=giohang',
					fields: [
								{ field: 'tennguoinhan', type: 'text', required: true, html: { caption: 'Tên người nhận', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'sodienthoai', type: 'number', required: true, html: { caption: 'Số ĐT', attr: 'style="width: 300px" placeholder="Bắt buộc,tối đa 12 ký tự"' } },
								{ field: 'diachi', type: 'textarea', required: true, html: { caption: 'Địa chỉ', attr: 'style="width: 300px" placeholder="Tối đa 250 kí tự"' }, options:{max:1} },

							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Lưu lại": function () {
									var error = this.validate();
									var self = this;
									if(error =='')
										$.post('ajax.php?act=giohang',{
											'cmd':'save-record',
											'record':this.record,
											}, 
											function(data){
												w2alert('<strong> Đặt hàng thành công <\/strong> <br>Mã đơn hàng của bạn là '+data+'<br>Vui lòng ghi lại mã đơn để tiện cho việc theo dõi');
											});
									 },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Tạo đơn hàng thành công<\/h5>", 3000);
										w2ui.loaihanghoagrid.reload();
										}
										},   
					  
					});
					
									
				}
				$().w2grid({
		name:"donhanggrid",
		url:'ajax.php?act=giohang',
		show: {
                toolbar: true,
                lineNumbers: true,
            },
		 toolbar: {
                items: [
                      { type: 'button', id: 'btnXoa', caption: 'Xóa', icon: 'glyphicon glyphicon-remove', hint: 'Xóa  hàng hóa đã chọn.' },

                ],
				 onClick: function (event) {
					 //them event cho toolbar
                   if (event.target == 'btnXoa') {
					   if(w2ui.donhanggrid.getSelection()=='')
					    {w2alert('Vui lòng chọn hàng hóa cần xóa');}
					   else{
					   	w2confirm('Bạn có chắc chắn muốn xóa ?', function (event) { 
						   if(event == "Yes") 
						   {
							w2ui.donhanggrid.delete(true);//hàm này kích hoạt sự kiện onDelete và tự đưa data lên url
							w2ui.donhanggrid.reload(); 
						   }
						   else
							{
								
								}

					   });}
                    }
                }
		 },
		columns:[
		{field:'hinh', caption:'Hình', size:'5%', render: function(hanghoa){
			if(hanghoa.hinh=="null")
				return ' ';
			else
			return "<img style=\"vertical-align:center;\" height=\"60\" src=\"Resource/HinhAnh/hanghoa/"+ hanghoa.hinh +"\"><\/img";
			}, style:'text-align:center'},
		{field:'mahanghoa', caption:'Mã',size:'10%'},
		{field:'tenhanghoa', caption:'Tên', size:'15%'},
		{field:'giaban', caption:'Giá', size:'10%',render:'float:0'}, //number_format
		{field:'soluong', caption:'Số lượng', size:'10%'},
		{field:'thanhtien', caption:'Thành tiền', size:'10%',render:'float:0'},
		],
		onDelete: function(event){
			//sau khi delete xong
			event.onComplete = function(){
				Materialize.toast("<h5>Xóa thành công<\/h5>", 3000);

				};
			},
			
		});
		var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
    $().w2layout({
        name: 'layoutDathang',
        panels: [          
            { type: 'main', style: pstyle },
			{ type: 'preview', size: 225, style: pstyle},
        ]
    });
			 $().w2popup('open', {
        title   : 'Thông tin đơn hàng',
        body    : '<div id="formDathang" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 600,
        height  : 700, 
        showMax : true,
        onToggle: function (event) {
            event.onComplete = function () {
                w2ui.layoutDathang.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
                $('#w2ui-popup #formDathang').w2render('layoutDathang');
				w2ui.layoutDathang.content('preview', w2ui.form);
                w2ui.layoutDathang.content('main', w2ui.donhanggrid);
            }
        }
    });
			}
	$('#btn-dathang').click(function(e) {
        openSuaPopup();
    });
});
</script>

<?php


if($listgiohang!=0)
{
$chuoi= <<<EOE
<table class="table table-bordered table-giohang">
  <thead>
    <tr>
      <th style="text-align:center">Hình ảnh</th>
      <th style="text-align:center">Tên hàng hóa</th>
      <th style="text-align:center">Giá bán</th>
      <th style="text-align:center">Số lượng</th>
      <th style="text-align:center">Thành tiền</th>
      <th style="text-align:center">Xóa</th>
    </tr>
  </thead>
  <tbody>
EOE;
echo $chuoi;
      foreach($listgiohang as $item)
	  {
		  $thanhtien = number_format($item['giaban'] * $item['soluong']);
		  $giaban = number_format($item['giaban']);
		$chuoi = <<<EOD
		<tr>
        	<td style="vertical-align:middle"><img height="15%" src="Resource/HinhAnh/hanghoa/{$item['hinh']}"/></td>
        	<td class="tenhanghoa" hanghoaid="{$item['hanghoaid']}" style="vertical-align:middle">{$item['tenhanghoa']}</td>
        	<td class="giaban" style="vertical-align:middle"">$giaban</td>
        	<td style="vertical-align:middle"><input class="soluong" type="number" value="{$item['soluong']}"/></td>
        	<td style="vertical-align:middle"><h4 class="thanhtien" style="display: inline-block;">{$thanhtien}</h4> vnd</td>
			<td style="vertical-align:middle">
			<a class="btnXoa btn btn-info" style="display:inline-block">
          		<span class="glyphicon glyphicon-remove"></span> 
        	</a>
			</td
      	</tr>
EOD;
      	echo $chuoi;
		  }
		  $chuoi = <<<EOA
	   </tbody>
	</table>

	<a style="float:right" id="btn-dathang" class=" btn btn-info" style="display:inline-block">
          		<span class="glyphicon glyphicon-saved"></span> Đặt hàng
        	</a>
EOA;
echo $chuoi;
}
else
{
	$chuoi = <<<EOD
	<div class="row">
  <div style="
	background-color: orange;
    padding-top: 10px;
    padding-bottom: 9px;
    margin-top: 35px;
    margin-right: 10px;
    color: white;
    font-size: large">Giỏ hàng còn trống</div>
</div>
EOD;

	echo $chuoi;
	}
	  ?>

