<script>
$(document).ready(function(e) {
	//tạo grid
	
    $("#loaihanghoagrid").w2grid({
		name:"loaihanghoagrid",
		url:'ajax.php?act=loaihanghoa',
		show: {
                toolbar: true,
                lineNumbers: true,
            },
		 toolbar: {
                items: [
                      { type: 'break' },
                      { type: 'button', id: 'btnThem', caption: 'Thêm', icon: 'glyphicon glyphicon-plus', hint: 'Thêm loại hàng hóa mới.' },
                      { type: 'break' },
                      { type: 'button', id: 'btnSua', caption: 'Sửa', icon: 'glyphicon glyphicon-pencil', hint: 'Sửa loại hàng hóa đã chọn.' },
                      { type: 'break' },
                      { type: 'button', id: 'btnXoa', caption: 'Xóa', icon: 'glyphicon glyphicon-remove', hint: 'Xóa loại hàng hóa đã chọn.' },

                ],
				 onClick: function (event) {
					 //them event cho toolbar
                    if (event.target == 'btnThem') {
                        openThemPopup();
                    } else if (event.target == 'btnSua') {
						if(w2ui.loaihanghoagrid.getSelection()=='')
					    {w2alert('Vui lòng chọn loại hàng hóa cần sửa');}
						else
                        openSuaPopup(w2ui.loaihanghoagrid.records[w2ui.loaihanghoagrid.getSelection(true)]);
                    } else if (event.target == 'btnXoa') {
					   if(w2ui.loaihanghoagrid.getSelection()=='')
					    {w2alert('Vui lòng chọn loại hàng hóa cần xóa');}
					   else{
					   	w2confirm('Bạn có chắc chắn muốn xóa ?', function (event) { 
						   if(event == "Yes") 
							w2ui.loaihanghoagrid.delete(true); //hàm này kích hoạt sự kiện onDelete và tự đưa data lên url
						   else
							{
								
								}

					   });}
                    }
                }
		 },
		columns:[
		{field:'icon', caption:'Icon',size:'7%', 
		 render: function(data){
			return "<img style=\"vertical-align:center;\" height=\"50\" src=\"Resource/HinhAnh/loaihanghoa/"+ data.icon +"\"><\/img";
			},
			style:'text-align:center'
			
		},
		{field:'maloaihanghoa', caption:'Mã loại',size:'30%'},
		{field:'tenloaihanghoa', caption:'Tên loại', size:'60%'},
		{ field: 'icon', type: 'file', required: true, html: { caption: 'Icon', attr: 'style="width: 300px" placeholder="Click để chọn icon"' }, options:{max:1} },

		],
		onDelete: function(event){
			//sau khi delete xong
			event.onComplete = function(){
				Materialize.toast("<h5>Xóa thành công<\/h5>", 3000);

				};
			}
		});
		
		function openSuaPopup(data){
			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formSua)
			{
				$().w2form({
					name:'formSua',
					
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=loaihanghoa',
					fields: [
								{ field: 'maloaihanghoa', type: 'text', required: true, html: { caption: 'Mã loại', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'tenloaihanghoa', type: 'text', required: true, html: { caption: 'Tên loại', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'hinh', type: 'file', required: true, html: { caption: 'Icon', attr: 'style="width: 300px" placeholder="Click để chọn icon"' }, options:{max:1} },

							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Lưu lại": function () { this.save({loai:'sua' /*post them du lieu o day*/}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Sửa thành công<\/h5>", 3000);
										w2ui.loaihanghoagrid.reload();
										}
										}
					  
					});
					
									
				}
			 $().w2popup('open', {
        title   : 'Sửa loại hàng hóa',
        body    : '<div id="formSua" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 300, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formSua.box).hide();
            event.onComplete = function () {
                $(w2ui.formSua.box).show();
                w2ui.formSua.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
				w2ui.formSua.record = data;
				w2ui.formSua.reload();
                $('#w2ui-popup #formSua').w2render('formSua');
            }
        }
    });
			}
			function openThemPopup(){
			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formThem)
			{
				$().w2form({
					name:'formThem',
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=loaihanghoa',
					fields: [
								{ field: 'maloaihanghoa', type: 'text', required: true, html: { caption: 'Mã loại', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'tenloaihanghoa', type: 'text', required: true, html: { caption: 'Tên loại', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'icon', type: 'file', required: true, html: { caption: 'Icon', attr: 'style="width: 300px" placeholder="Click để chọn icon"' }, options:{max:1} },
							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Thêm": function () { this.save({loai:'them'}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Thêm thành công<\/h5>", 3000);
										w2ui.loaihanghoagrid.reload();
										}
										}
					  
					});
					
									
				}
			 $().w2popup('open', {
        title   : 'Thêm loại hàng hóa',
        body    : '<div id="formThem" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 250, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formThem.box).hide();
            event.onComplete = function () {
                $(w2ui.formThem.box).show();
                w2ui.formThem.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
                $('#w2ui-popup #formThem').w2render('formThem');
            }
        }
    });
			}
			
		
});

</script>

<div id="quanlynhomhanghoa" class="box box-inverse">
  <div class="box-title">
    <h3> <i class="fa fa-list"></i> Quản lý nhóm hàng hóa </h3>
  </div>
  <div id="loaihanghoagrid" style="width: 100%; height:100%;"></div>
</div>
