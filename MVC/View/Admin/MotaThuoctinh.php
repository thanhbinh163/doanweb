<script>
$(document).ready(function(e) {
	//tạo grid
    configHanghoagrid = {
		name:"hanghoagrid",
		url:'ajax.php?act=hanghoa',
		show: {
                toolbar: false,
                lineNumbers: true,
            },
			
		columns:[
		{field:'hinh', caption:'Hình', size:'5%', render: function(hanghoa){
			return "<img style=\"vertical-align:center;\" height=\"60\" src=\"Resource/HinhAnh/hanghoa/"+ hanghoa.hinh +"\"><\/img";
			}, style:'text-align:center'},
		{field:'mahanghoa', caption:'Mã',size:'10%'},
		{field:'tenhanghoa', caption:'Tên', size:'15%'},
		{field:'giaban', caption:'Giá', size:'10%',render:'float:0'}, //number_format
		{field:'namsanxuat', caption:'Năm SX', size:'5%'},
		{field:'tennhasanxuat', caption:'Nhà SX', size:'7%'},
		{field:'tenloaihanghoa', caption:'Loại HH', size:'10%'},
		{field:'mota', caption:'Mô tả', size:'15%'},
		],
		onSelect : function(event){
			w2ui.motathuoctinhgrid.routeData.hanghoaid = event.recid;//thay doi hanghoaid trong routeData dung de getlist
			w2ui.motathuoctinhgrid.reload();
			},
		onRefresh: function(event){
			w2ui.hanghoagrid.select(w2ui.hanghoagrid.records[0].recid);
			},
		};
		
	configMotathuoctinhgrid = {
		name:"motathuoctinhgrid",
		url: 'ajax.php?act=motathuoctinh&Hanghoaid=:hanghoaid', //dynamic url
		routeData: { hanghoaid: '1' },//data de getlist
		show: {
                toolbar: true,
                lineNumbers: true,
            },
		 toolbar: {
                items: [
                      { type: 'break' },
                      { type: 'button', id: 'btnThem', caption: 'Thêm', icon: 'glyphicon glyphicon-plus', hint: 'Thêm thuộc tính mới.' },
                      { type: 'break' },
                      { type: 'button', id: 'btnSua', caption: 'Sửa', icon: 'glyphicon glyphicon-pencil', hint: 'Sửa thuộc tính đã chọn.' },
                      { type: 'break' },
                      { type: 'button', id: 'btnXoa', caption: 'Xóa', icon: 'glyphicon glyphicon-remove', hint: 'Xóa thuộc tính đã chọn.' },
					  { type: 'break' },
                      { type: 'button', id: 'btnXoatatca', caption: 'Xóa tất cả', icon: 'glyphicon glyphicon-remove', hint: 'Xóa tất cả thuộc tính' },

                ],
				 onClick: function (event) {
					 //them event cho toolbar
                    if (event.target == 'btnThem') {
						if(w2ui.hanghoagrid.getSelection()=='')
					    {w2alert('Vui lòng chọn hàng hóa cần thêm thuộc tính');}
						else
                        openThemPopup();
                    } else if (event.target == 'btnSua') {
						if(w2ui.motathuoctinhgrid.getSelection()=='')
					    {w2alert('Vui lòng chọn thuộc tính cần sửa');}
						else
                        openSuaPopup(w2ui.motathuoctinhgrid.records[w2ui.motathuoctinhgrid.getSelection(true)]);
                    } else if (event.target == 'btnXoatatca') {
						w2ui.motathuoctinhgrid.request('delete-all-records',{params:{hanghoaid:w2ui.hanghoagrid.getSelection()}});
						
	
                    } else if (event.target == 'btnXoa') {
					   if(w2ui.motathuoctinhgrid.getSelection()=='')
					    {w2alert('Vui lòng chọn thuộc tính cần xóa');}
					   else{
					   	w2confirm('Bạn có chắc chắn muốn xóa ?', function (event) { 
						   if(event == "Yes") 
							w2ui.motathuoctinhgrid.delete(true); //hàm này kích hoạt sự kiện onDelete và tự đưa data lên url
						   else
							{
								
								}

					   });}
                    }
                }
		 },
		columns:[
		{field:'tenthuoctinh', caption:'Tên thuộc tính',size:'30%'},
		{field:'mota', caption:'Mô tả', size:'70%'},
		],
		onDelete: function(event){
			//sau khi delete xong
			event.onComplete = function(){
				Materialize.toast("<h5>Xóa thành công<\/h5>", 3000);

				};
			},
			

		};
		
		function openSuaPopup(data){
			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formSua)
			{
				$().w2form({
					name:'formSua',
					
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=motathuoctinh',
					fields: [
								{ field: 'tenthuoctinh', type: 'text', html: { caption: 'Thuộc tính', attr: 'readonly style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'mota', type: 'text', required: true, html: { caption: 'Mô tả', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Lưu lại": function () { this.save({loai:'sua' /*post them du lieu o day*/}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Sửa thành công<\/h5>", 3000);
										w2ui.motathuoctinhgrid.reload();
										}
										}
					  
					});
					
									
				}
			 $().w2popup('open', {
        title   : 'Sửa thuộc tính',
        body    : '<div id="formSua" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 200, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formSua.box).hide();
            event.onComplete = function () {
                $(w2ui.formSua.box).show();
                w2ui.formSua.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
				w2ui.formSua.record = data;
				w2ui.formSua.reload();
                $('#w2ui-popup #formSua').w2render('formSua');
            }
        }
    });
			}
			function openThemPopup(){

			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formThem)
			{
				$().w2form({
					name:'formThem',
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=motathuoctinh',
					fields: [
								{ field: 'thuoctinhid', type: 'list', required: true,options:{items:<?php echo json_encode($listthuoctinh,JSON_UNESCAPED_UNICODE); ?>,}, html: { caption: 'Mã thuộc tính', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'mota', type: 'text', required: true, html: { caption: 'Tên thuộc tính', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Thêm": function () { this.save({loai:'them', hanghoaid:w2ui.hanghoagrid.getSelection()}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Thêm thành công<\/h5>", 3000);
										w2ui.motathuoctinhgrid.reload();
										this.clear();
										}
										}
					  
					});
					
									
				}
			 $().w2popup('open', {
        title   : 'Thêm thuộc tính',
        body    : '<div id="formThem" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 200, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formThem.box).hide();
            event.onComplete = function () {
                $(w2ui.formThem.box).show();
                w2ui.formThem.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
                $('#w2ui-popup #formThem').w2render('formThem');
            }
        }
    });
			}
		
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
    $('#motathuoctinhlayout').w2layout({
        name: 'motathuoctinhlayout',
        padding: 0,
        panels: [
            { type: 'right',minSize: 200, style:pstyle, size:'500'},
            { type: 'main', minSize: 200, content: w2ui.hanghoagrid , style:pstyle }
        ],
		
    });
	var hanghoagrid = $().w2grid(configHanghoagrid);
	w2ui.hanghoagrid.reload();
	w2ui.motathuoctinhlayout.content('main', hanghoagrid);
	w2ui.motathuoctinhlayout.content('right', $().w2grid(configMotathuoctinhgrid));
});

</script>

<div id="quanlyhanghoa" class="box box-inverse">
  <div class="box-title">
    <h3> <i class="fa fa-list"></i> Quản lý hàng hóa </h3>
  </div>
  <div id="motathuoctinhlayout" style="width: 100%; height:100%;"></div>
</div>
