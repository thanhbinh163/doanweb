<script>
$(document).ready(function(e) {
	//tạo grid
    $("#hanghoagrid").w2grid({
		name:"hanghoagrid",
		url:'ajax.php?act=hanghoa',
		show: {
                toolbar: true,
                lineNumbers: true,
            },
		 toolbar: {
                items: [
                      { type: 'break' },
                      { type: 'button', id: 'btnThem', caption: 'Thêm', icon: 'glyphicon glyphicon-plus', hint: 'Thêm loại hàng hóa mới.' },
                      { type: 'break' },
                      { type: 'button', id: 'btnSua', caption: 'Sửa', icon: 'glyphicon glyphicon-pencil', hint: 'Sửa loại hàng hóa đã chọn.' },
                      { type: 'break' },
                      { type: 'button', id: 'btnXoa', caption: 'Xóa', icon: 'glyphicon glyphicon-remove', hint: 'Xóa loại hàng hóa đã chọn.' },

                ],
				 onClick: function (event) {
					 //them event cho toolbar
                    if (event.target == 'btnThem') {
                        openThemPopup();
                    } else if (event.target == 'btnSua') {
						if(w2ui.hanghoagrid.getSelection()=='')
					    {w2alert('Vui lòng chọn loại hàng hóa cần sửa');}
						else
                        openSuaPopup(w2ui.hanghoagrid.records[w2ui.hanghoagrid.getSelection(true)]);
                    } else if (event.target == 'btnXoa') {
					   if(w2ui.hanghoagrid.getSelection()=='')
					    {w2alert('Vui lòng chọn loại hàng hóa cần xóa');}
					   else{
					   	w2confirm('Bạn có chắc chắn muốn xóa ?', function (event) { 
						   if(event == "Yes") 
							w2ui.hanghoagrid.delete(true); //hàm này kích hoạt sự kiện onDelete và tự đưa data lên url
						   else
							{
								
								}

					   });}
                    }
                }
		 },
		columns:[
		{field:'hinh', caption:'Hình', size:'5%', render: function(hanghoa){
			
			return "<img style=\"vertical-align:center;\" height=\"60\" src=\"Resource/HinhAnh/hanghoa/"+ hanghoa.hinh +"\"><\/img";
			}, style:'text-align:center'},
		{field:'mahanghoa', caption:'Mã',size:'10%'},
		{field:'tenhanghoa', caption:'Tên', size:'15%'},
		{field:'giaban', caption:'Giá', size:'10%',render:'float:0'}, //number_format
		{field:'namsanxuat', caption:'Năm SX', size:'5%'},
		{field:'tennhasanxuat', caption:'Nhà SX', size:'7%'},
		{field:'tenloaihanghoa', caption:'Loại HH', size:'10%'},
		{field:'mota', caption:'Mô tả', size:'15%'},
		],
		onDelete: function(event){
			//sau khi delete xong
			event.onComplete = function(){
				Materialize.toast("<h5>Xóa thành công<\/h5>", 3000);

				};
			},
			onRender: function(event){
    		this.sort('recid', 'asc');
}
			
		});
		
		function openSuaPopup(data){
			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formSua)
			{
				$().w2form({
					name:'formSua',
					
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=hanghoa',
					fields: [
								{ field: 'loaihanghoaid', type: 'list', required: true,options:{items:<?php echo json_encode($listloaihanghoa,JSON_UNESCAPED_UNICODE); ?>, placeholder:'Click để chọn loại hàng hóa - Bắt buộc'}, html: { caption: 'Loại hàng hóa'}},
								{ field: 'nhasanxuatid', type: 'list', required: true,options:{items:<?php echo json_encode($listnhasanxuat,JSON_UNESCAPED_UNICODE); ?>, placeholder:'Click để chọn nhà sản xuất - Bắt buộc'}, html: { caption: 'Nhà sản xuất'}},
								{ field: 'mahanghoa', type: 'text', required: true, html: { caption: 'Mã Hàng hóa', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'tenhanghoa', type: 'text', required: true, html: { caption: 'Tên Hàng hóa', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'giaban', type: 'int', required: true, html: { caption: 'Giá bán', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'namsanxuat',required: true, type: 'hex', options:{min:0} ,html: { caption: 'Năm sản xuất', attr: 'style="width: 300px" placeholder="Nhập số dương"' } },
								{ field: 'hinhsua1', type: 'file', html: { caption: 'Hình ảnh 1', attr: 'style="width: 300px; height:50px;" placeholder="Click để chọn ảnh"' }},
								{ field: 'hinhsua2', type: 'file', html: { caption: 'Hình ảnh 2', attr: 'style="width: 300px; height:50px;" placeholder="Click để chọn ảnh"' }},
								{ field: 'hinhsua3', type: 'file', html: { caption: 'Hình ảnh 3', attr: 'style="width: 300px; height:50px;" placeholder="Click để chọn ảnh"' }},
								{ field: 'hinhsua4', type: 'file', html: { caption: 'Hình ảnh 4', attr: 'style="width: 300px; height:50px;" placeholder="Click để chọn ảnh"' }},
								{ field: 'mota', type: 'textarea', html: { caption: 'Mô tả', attr: 'style="width: 300px; height:50px;" placeholder="Tối da 250 ký tự"' } },
							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Lưu lại": function () { this.save({loai:'sua' /*post them du lieu o day*/}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Sửa thành công<\/h5>", 3000);
										w2ui.hanghoagrid.reload();
										}
										}
					  
					});
					
									
				}
			 $().w2popup('open', {
        title   : 'Sửa loại hàng hóa',
        body    : '<div id="formSua" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 700, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formSua.box).hide();
            event.onComplete = function () {
                $(w2ui.formSua.box).show();
                w2ui.formSua.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
				w2ui.formSua.record = data;
				w2ui.formSua.reload();
                $('#w2ui-popup #formSua').w2render('formSua');
            }
        }
    });
			}
			function openThemPopup(){
				var i = 1;
			var fieldOptions=[
								{ field: 'loaihanghoaid', type: 'list', required: true,options:{items:<?php echo json_encode($listloaihanghoa,JSON_UNESCAPED_UNICODE); ?>, placeholder:'Click để chọn loại hàng hóa - Bắt buộc'}, html: { caption: 'Loại hàng hóa'}},
								{ field: 'nhasanxuatid', type: 'list', required: true,options:{items:<?php echo json_encode($listnhasanxuat,JSON_UNESCAPED_UNICODE); ?>, placeholder:'Click để chọn nhà sản xuất - Bắt buộc'}, html: { caption: 'Nhà sản xuất'}},
								{ field: 'mahanghoa', type: 'text', required: true, html: { caption: 'Mã Hàng hóa', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'tenhanghoa', type: 'text', required: true, html: { caption: 'Tên Hàng hóa', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'giaban', type: 'int', required: true, html: { caption: 'Giá bán', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'namsanxuat', type: 'hex', options:{min:0} ,html: { caption: 'Năm sản xuất', attr: 'style="width: 300px" placeholder="Nhập số dương"' } },
								{ field: 'hinh', type: 'file', required: true, html: { caption: 'Hình ảnh', attr: 'style="width: 300px; height:50px;" placeholder="Click để chọn ảnh - Bắt buộc, tối đa 4 ảnh"' }, 
								  options:{max:4,}
								 },
								{ field: 'mota', type: 'textarea', html: { caption: 'Mô tả', attr: 'style="width: 300px; height:50px;" placeholder="Tối da 250 ký tự"' } },
								{ field: 'thuoctinh',required: true,html: { caption: 'Thuộc tính', attr: 'style="width: 300px; height:50px;"' }, type: 'enum',
								options:{
									items:<?php echo json_encode($listthuoctinh,JSON_UNESCAPED_UNICODE); ?>, 
									openOnFocus:true,
									style:'width:300px',
									placeholder:'Click để chọn thuộc tính - Bắt buộc',
									onAdd: function(event){
										event.onComplete = function(){		
										formOptions.fields.push({field: 'tt'+event.item.id, type: 'text', required: true, html: { caption: event.item.text}});
										var e = w2ui.formThem.record;
										w2ui.formThem.destroy();
										$().w2form(formOptions);
										w2ui.formThem.record = e;
										w2ui.formThem.reload();
										$('#w2ui-popup #formThem').w2render('formThem');
											};
										
										},
									onRemove: function(event){
										
										event.onComplete = function(){
											var index = formOptions.fields.findIndex(function (data){ 
												var tenfield = 'tt'+event.item.id;
												return data.field == tenfield ;
												});
										if((i%2)!=0) //su kien onRemove dc goi 2 lan
											formOptions.fields.splice(index,1);
										i++;
										var e = w2ui.formThem.record;
										w2ui.formThem.destroy();
										$().w2form(formOptions);
										w2ui.formThem.record = e;
										$('#w2ui-popup #formThem').w2render('formThem');
											};
										
										},
									}, 
								html: { caption: 'Thuộc tính'}
								},
							];
							var formOptions = {
					name:'formThem',
					style: 'border: 0px; background-color: transparent;',
					url:'ajax.php?act=hanghoa',
					focus: 9,
					fields: [
								{ field: 'loaihanghoaid', type: 'list', required: true,options:{items:<?php echo json_encode($listloaihanghoa,JSON_UNESCAPED_UNICODE); ?>, placeholder:'Click để chọn loại hàng hóa - Bắt buộc'}, html: { caption: 'Loại hàng hóa'}},
								{ field: 'nhasanxuatid', type: 'list', required: true,options:{items:<?php echo json_encode($listnhasanxuat,JSON_UNESCAPED_UNICODE); ?>, placeholder:'Click để chọn nhà sản xuất - Bắt buộc'}, html: { caption: 'Nhà sản xuất'}},
								{ field: 'mahanghoa', type: 'text', required: true, html: { caption: 'Mã Hàng hóa', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 50 ký tự"' } },
								{ field: 'tenhanghoa', type: 'text', required: true, html: { caption: 'Tên Hàng hóa', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'giaban', type: 'int', required: true, html: { caption: 'Giá bán', attr: 'style="width: 300px" placeholder="Bắt buộc,tối da 100 ký tự"' } },
								{ field: 'namsanxuat', type: 'hex', options:{min:0} ,html: { caption: 'Năm sản xuất', attr: 'style="width: 300px" placeholder="Nhập số dương"' } },
								{ field: 'hinh', type: 'file', required: true, html: { caption: 'Hình ảnh', attr: 'style="width: 300px; height:50px;" placeholder="Click để chọn ảnh - Bắt buộc, tối đa 4 ảnh"' }, 
								  options:{max:4,}
								 },
								{ field: 'mota', type: 'textarea', html: { caption: 'Mô tả', attr: 'style="width: 300px; height:50px;" placeholder="Tối da 250 ký tự"' } },
								{ field: 'thuoctinh',required: true,html: { caption: 'Thuộc tính', attr: 'style="width: 300px; height:50px;"' }, type: 'enum',
								options:{
									items:<?php echo json_encode($listthuoctinh,JSON_UNESCAPED_UNICODE); ?>, 
									openOnFocus:true,
									style:'width:300px',
									placeholder:'Click để chọn thuộc tính - Bắt buộc',
									onAdd: function(event){
										event.onComplete = function(){		
										formOptions.fields.push({field: 'tt'+event.item.id, type: 'text', required: true, html: { caption: event.item.text}});
										var e = w2ui.formThem.record;
										w2ui.formThem.destroy();
										$().w2form(formOptions);
										w2ui.formThem.record = e;
										w2ui.formThem.reload();
										$('#w2ui-popup #formThem').w2render('formThem');
											};
										
										},
									onRemove: function(event){
										
										event.onComplete = function(){
											var index = formOptions.fields.findIndex(function (data){ 
												var tenfield = 'tt'+event.item.id;
												return data.field == tenfield ;
												});
										if((i%2)!=0) //su kien onRemove dc goi 2 lan
											formOptions.fields.splice(index,1);
										i++;
										var e = w2ui.formThem.record;
										w2ui.formThem.destroy();
										$().w2form(formOptions);
										w2ui.formThem.record = e;
										$('#w2ui-popup #formThem').w2render('formThem');
											};
										
										},
									}, 
								html: { caption: 'Thuộc tính'}
								},
							],
					actions: {
								"Làm mới": function () { this.clear(); },
								"Thêm": function () { this.save({loai:'them'}); },
							   },
							   //onSave đc kích hoạt khi gọi save()
								   onSave:function(event){
									   event.onComplete = function(){
										w2popup.close();
										Materialize.toast("<h5>Thêm thành công<\/h5>", 3000);
										w2ui.hanghoagrid.reload();
										}
										}
					  
					}
			//nếu chưa có formThem thì tạo mới
			if(!w2ui.formThem)
			{
				$().w2form(formOptions);			
				}
			 $().w2popup('open', {
        title   : 'Thêm loại hàng hóa',
        body    : '<div id="formThem" style="width: 100%; height: 100%;"></div>',
        style   : 'padding: 15px 0px 0px 0px',
        width   : 500,
        height  : 700, 
        showMax : true,
        onToggle: function (event) {
            $(w2ui.formThem.box).hide();
            event.onComplete = function () {
                $(w2ui.formThem.box).show();
                w2ui.formThem.resize();
            }
        },
        onOpen: function (event) {
            event.onComplete = function () {
				//sau khi đã mở lên thì render formThem vào #formThem
				w2ui.formThem.clear();
                $('#w2ui-popup #formThem').w2render('formThem');
            }
        },
		onClose: function(event){
			w2ui.formThem.destroy();
			formOptions.fields= fieldOptions;
			$().w2form(formOptions);
			
			}
    });
			}
		
});

</script>

<div id="quanlyhanghoa" class="box box-inverse">
  <div class="box-title">
    <h3> <i class="fa fa-list"></i> Quản lý hàng hóa </h3>
  </div>
  <div id="hanghoagrid" style="width: 100%; height:100%;"></div>
</div>
