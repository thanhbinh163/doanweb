<script>
$(document).ready(function(e) {
	
	//tao form
	
	configphieugiaohangform = {
		
		name:"phieugiaohangform",
		header : 'Phiếu giao hàng',
		recid:1,
		url: 'ajax.php?act=phieugiaohang&donhangid=:donhangid', //dynamic url
		routeData: { donhangid: null },//data de get
		fields:[
				{ field: 'tennguoinhan', type: 'text', html: { caption: 'Tên người nhận', attr: 'style="width: 300px" placeholder="Bắt buộc"' } },
				{ field: 'sodienthoai', type: 'text',html: { caption: 'Số ĐT', attr: 'style="width: 300px" placeholder="Bắt buộc"' } },
				{ field: 'diachi', type: 'textarea',html: { caption: 'Địa chỉ', attr: 'style="width: 300px" placeholder="Bắt buộc"' } },
				{ field: 'phigiaohang', type: 'text',html: { caption: 'Phí giao hàng', attr: 'style="width: 300px" placeholder=""' } },
			],
		}
		;
	
	//tạo grid
    configdonhanggrid = {
		name:"donhanggrid",
		url:'ajax.php?act=donhang&loai=getdonhangcangiao',
		show: {
                toolbar: true,
                lineNumbers: true,
				selectColumn: true
            },
		multiSelect: true,
		toolbar: {
                items: [
                      { type: 'break' },
                      { type: 'button', id: 'btnSelectAll', caption: 'Chọn tất cả', icon: 'glyphicon glyphicon-ok', hint: 'Chọn tất cả' },
                      { type: 'break' },
					  { type: 'button', id: 'btnDeselectAll', caption: 'Bỏ chọn', icon: 'glyphicon glyphicon-remove', hint: 'Bỏ chọn' },
                      { type: 'break' },

                ],
				 onClick: function (event) {
                    if (event.target == 'btnSelectAll') {
						w2ui.donhanggrid.selectAll();
						}
					else if (event.target == 'btnDeselectAll') {
						w2ui.donhanggrid.selectNone();
						}
					}
		},
					
		columns:[
		{field:'madonhang', caption:'Mã',size:'10%'},
		{field:'ngaylap', caption:'Ngày lập', size:'15%'},
		{field:'tongtien', caption:'Tổng tiền', size:'10%',render:'float:0'}, //number_format
		{field:'trangthai', caption:'Hình', size:'10%', render: function(donhang){
			if(donhang.trangthai ==0)
				return "Đơn mới";
			else if(donhang.trangthai==1)
				return "Đã xác nhận";
			else
				return "Giao hàng";
			}, style:'text-align:center'},
		],
		onSelect : function(event){
			w2ui.chitietdonhanggrid.routeData.donhangid = event.recid;//thay doi donhangid trong routeData dung de getlist
			w2ui.phieugiaohangform.routeData.donhangid = event.recid;
			w2ui.phieugiaohangform.reload();
			w2ui.chitietdonhanggrid.reload();
			w2ui.chitietdonhanggrid.on('error', function(event) {
					event.preventDefault();
				});
			},
		onRefresh: function(event){
			if(w2ui.donhanggrid.records[0] != null)
			{
				w2ui.donhanggrid.select(w2ui.donhanggrid.records[0].recid);
			}
			else
			{
				w2ui.chitietdonhanggrid.on('error', function(event) {
					event.preventDefault();
				});
				}
			},
		};
		
	configchitietdonhanggrid = {
		name:"chitietdonhanggrid",
		url: 'ajax.php?act=chitietdonhang&donhangid=:donhangid', //dynamic url
		routeData: { donhangid: null },//data de getlist
		show: {
                toolbar: true,
                lineNumbers: true,
            },
		 toolbar: {
                items: [
                      { type: 'break' },
                      { type: 'button', id: 'btnXacnhan', caption: 'Xác nhận Đang giao hàng', icon: 'glyphicon glyphicon-ok', hint: 'Xác nhận đơn hàng đang được giao' },
                      { type: 'break' },

                ],
				 onClick: function (event) {
					 //them event cho toolbar
                    if (event.target == 'btnXacnhan') {
						if(w2ui.donhanggrid.getSelection()=='')
					    {w2alert('Vui lòng chọn đơn hàng cần giao');}
						else
                        {
							 if(w2ui['phieugiaohangform'].validate(true) == '')
						   {
							   if(w2ui.donhanggrid.getSelection(true).length==1){
								   var madonhang = w2ui.donhanggrid.records[w2ui.donhanggrid.getSelection(true)].madonhang;
								   }
							   else {
								   var madonhang ='';
								   w2ui.donhanggrid.getSelection(true).forEach(function(value, key){
									   
									   madonhang += w2ui.donhanggrid.records[value].madonhang + ', ';
									   });
								   }
							w2confirm('Bạn chọn mã đơn hàng: <strong>'+ madonhang + '<\/strong> <br> Bạn có muốn chuyển sang trạng thái Đang giao hàng ?', function (event) { 
						   if(event == "Yes") {
						  	
							if(w2ui.donhanggrid.getSelection(true).length==1){
								   $.post("ajax.php?act=donhang",{
									cmd:'save-record',
									loai:'suatrangthai-dagiaohang',
									record:w2ui.donhanggrid.get(w2ui.donhanggrid.getSelection()),
									phieugiaohang:w2ui.phieugiaohangform.record,
								}, function(){w2ui.donhanggrid.reload();});
								   }
							   else {
								  $.post("ajax.php?act=donhang",{
									cmd:'save-record',
									listdonhangid:w2ui.donhanggrid.getSelection(),
									loai:'suatrangthai-dagiaohang-list',
									record:w2ui.donhanggrid.get(w2ui.donhanggrid.getSelection()),
									phieugiaohang:w2ui.phieugiaohangform.record,
								}, function(){w2ui.donhanggrid.reload();});
								   }
						   }

						   else
							{

								
								}

					   });}
							}
                    } 
                }
		 },
		columns:[
		{field:'hinh', caption:'Hình',size:'10%'},
		{field:'mahanghoa', caption:'Mã', size:'5%'},
		{field:'tenhanghoa', caption:'Tên', size:'15%'},
		{field:'tenloaihanghoa', caption:'Loại', size:'10%'},
		{field:'tennhasanxuat', caption:'NSX', size:'10%'},
		{field:'giaban', caption:'Giá bán', size:'15%', render:'float:0'},
		{field:'soluong', caption:'Số lượng', size:'5%'},
		{field:'thanhtien', caption:'Thành tiền', size:'15%', render : function(chitiethoadon){ return numeral(chitiethoadon.giaban * chitiethoadon.soluong).format('0,0'); }},
		],
		onDelete: function(event){
			//sau khi delete xong
			event.onComplete = function(){
				Materialize.toast("<h5>Xóa thành công<\/h5>", 3000);

				};
			},
			

		};
		
	var donhanggrid = $().w2grid(configdonhanggrid);
	w2ui.donhanggrid.reload();
	var chitietdonhanggrid = $().w2grid(configchitietdonhanggrid);
	var phieugiaohangform = $().w2form(configphieugiaohangform);
	var pstyle = 'border: 1px solid #dfdfdf; padding: 5px;';
    $('#chitietdonhanglayout').w2layout({
        name: 'chitietdonhanglayout',
        padding: 0,
        panels: [
            { type: 'main',minSize: 200, style:pstyle,},
            { type: 'left', minSize: 200,size:'500',style:pstyle },
			{ type: 'preview', minSize: 200,size:'60%',style:pstyle }
        ],
		
    });
	w2ui.chitietdonhanglayout.content('left', donhanggrid);
	w2ui.chitietdonhanglayout.content('preview', chitietdonhanggrid);
	w2ui.chitietdonhanglayout.content('main', phieugiaohangform);
	//console.log(w2ui.donhanggrid.select(w2ui.donhanggrid.records[0].recid));

});
</script>

<div id="quanlydonhang" class="box box-inverse">
  <div class="box-title">
    <h3> <i class="fa fa-list"></i> Quản lý hàng hóa </h3>
  </div>
  <div id="chitietdonhanglayout" style="width: 100%; height:100%;"></div>
</div>
