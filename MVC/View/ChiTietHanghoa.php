<link rel="stylesheet" type="text/css" href="./Plugin/slick-1.6.0/slick/slick.css">
<link rel="stylesheet" type="text/css" href="./Plugin/slick-1.6.0/slick/slick-theme.css">
<script src="./Plugin/slick-1.6.0/slick/slick.js"></script>
<script>  

$(document).ready(function(e) {
    $("#ThemGioHangbtn").click(function(e) {
        var self=this;
		var sl = $("#soluong").val();
		$.post("ajax.php?act=giohang",{
			loai:'them',
			hanghoaid:$(self).attr('hanghoaid'),
			soluong:sl
			}, function(){Materialize.toast('Thêm vào giỏ hàng thành công', 2000);});
		
    });
	$('.myCarousel').slick({
  dots: true,
  infinite: true,
  speed: 1000,
  fade: true,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 1500,
});
});
</script>


<div class="container-fluid">
  <div class="row">
    <div class="col-lg-4 hinhanh">
      <h2>
      <?php echo $hanghoa['tenhanghoa'];?>
      </h3>
      
      <div class="myCarousel">
      <div><img style="margin: 0 auto;" src="Resource/HinhAnh/hanghoa/<?php echo $hanghoa['hinh'];?>" class="img-responsive"/></div>
      <?php
      if($hanghoa['hinh2']!='')
	  {
	   $chuoi = <<<EOS
	   <div><img style="margin: 0 auto;" src="Resource/HinhAnh/hanghoa/{$hanghoa['hinh2']}" class="img-responsive"/></div>
EOS;
		echo $chuoi;
	  }
	  if($hanghoa['hinh3']!='')
	  {
	   $chuoi = <<<EOS
	   <div><img style="margin: 0 auto;" src="Resource/HinhAnh/hanghoa/{$hanghoa['hinh3']}" class="img-responsive"/></div>
EOS;
		echo $chuoi;
	  }
	  if($hanghoa['hinh4']!='')
	  {
	   $chuoi = <<<EOS
	   <div><img style="margin: 0 auto;" src="Resource/HinhAnh/hanghoa/{$hanghoa['hinh4']}" class="img-responsive"/></div>
EOS;
		echo $chuoi;
	  }
	  ?>
      </div>
      </div>
    <div class="col-lg-7 col-lg-offset-1" style="text-align:left; margin-top:3%">
      <h3> Giá bán: <?php echo number_format($hanghoa['giaban']);?> vnd</h3>
      <h4>Số lượng :
        <input type="number" value="1" id="soluong" style="position:absolute" class="col-lg-2"/>
      </h4>
      <a class="btn btn-info btn-dathang" id="ThemGioHangbtn" hanghoaid="<?php echo $_REQUEST['hanghoaid']; ?>">Thêm vào giỏ hàng</a>
      <h3>Thông Số</h3>
      <div class="col-lg-6">
        <table class="table table-striped table-bordered">
          <thead >
            <tr>
              <th>Tên thuộc tính</th>
              <th>Mô tả</th>
            </tr>
          </thead>
          <tbody>
            <!--Bắt đầu 1 dòng-->
            <?php
			foreach($listthuoctinh as $item)
			{
			$chuoi = <<<EOD
            <tr>
              <td>{$item['tenthuoctinh']}</td>
              <td>{$item['mota']}</td>
            </tr>
EOD;
			echo $chuoi;
			}
			?>
            <!--Kết thúc 1 dòng-->
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row">
  <hr style="width:90%">
  <div class="container" id="motaContainer">
  </div>
  </div>
</div>
