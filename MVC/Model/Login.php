<?php
include_once("DataProvider.php");
class User
{
	private $db;

	function __construct()
	{
		$this->db = new DataProvider();
	}

	function FindUser($sql)
	{

		return $this->db->NumRows($sql);
	}
	function Execute($sql)
	{
		return $this->db->ExecuteQuery($sql);
	}

}
?>