<?php
include_once("DataProvider.php");
class Hanghoa
{
	private $db;
	function __construct()
	{
		$this->db = new DataProvider(); 
	}
	
	function SelectHanghoa($sql)
	{
		return $this->db->FetchAll($sql);
	}
	function GetCountAllHanghoa()
	{
		return $this->db->NumRows("select * from hanghoa");
		}
	function GetCountHangHoaby($sql)
	{
		return $this->db->NumRows($sql);
	}
	function counthanghoabymahanghoa($mahanghoa){
		return $this->db->NumRows("select * from hanghoa where mahanghoa='$mahanghoa'");
		}
	
	function getlisthanghoagrid()
	{
		$tmp = $this->db->FetchAll("select hanghoaid, mahanghoa, tenhanghoa, giaban, namsanxuat, hinh,hinh2,hinh3,hinh4, mota, hh.nhasanxuatid ,tennhasanxuat, hh.loaihanghoaid, tenloaihanghoa from hanghoa hh join nhasanxuat nsx on hh.nhasanxuatid=nsx.nhasanxuatid join loaihanghoa lhh on hh.loaihanghoaid=lhh.loaihanghoaid");
		$listhanghoa = array();
		foreach($tmp as $hanghoa)
		{
   			$hanghoa['recid'] = $hanghoa['hanghoaid'];
			$listhanghoa[]=$hanghoa;
		}
		return $listhanghoa;
		}
	function gethanghoabyhanghoaid($hanghoaid)
	{
		$sql = "select * from hanghoa where hanghoaid = $hanghoaid";
		return $this->db->Fetch($sql);
		}
	function xoahanghoa($hanghoaid)
	{
		//xoa thuoc tinh
		$sql="delete from motathuoctinh where hanghoaid = $hanghoaid";
		$this->db->ExecuteQuery($sql);
		
		//xoa hinh
		$hanghoa = $this->gethanghoabyhanghoaid($hanghoaid);
		for($i = 1; $i<=4; $i++)
		{
			$path='';
			if($i==1)
			{
				if($hanghoa['hinh'] != '')
				{
					$path="Resource/HinhAnh/hanghoa/";
				$path .= $hanghoa['hinh'];
								}
			}
			else
			{
				if($hanghoa['hinh'.$i] != '')
				{
					$path="Resource/HinhAnh/hanghoa/";
					$path .= $hanghoa['hinh'.$i];
				}
			}
			if(file_exists($path) && $path != '')
				unlink($path);
			}
		//xoa hanghoa
		$sql="delete from hanghoa where hanghoaid = $hanghoaid";
		$this->db->ExecuteQuery($sql);

		return $hanghoaid;
		}	
	
	function themhanghoa($mahanghoa, $tenhanghoa, $giaban, $namsanxuat, $nhasanxuatid, $loaihanghoaid, $mota, $listhinh, $listthuoctinh)
	{
		//tạo mảng tên hình ảnh
		$i=1;
	    $prefixtenhinh = str_replace(' ', '', $tenhanghoa).'_';
	    $listtenhinh = array();
	    foreach($listhinh as $hinh)
		{
			$type=str_replace('image/', '', $hinh['type']);
			$filename = $prefixtenhinh.$i++.'.'.$type;
			$listtenhinh[]=$filename;
			
		  }
		  
		//tạo câu lệnh sql insert cho hang hoa
		$insertcolumns = "INSERT INTO hanghoa (mahanghoa,tenhanghoa,giaban,namsanxuat, nhasanxuatid, loaihanghoaid, mota";
		$values =" VALUES ('$mahanghoa','$tenhanghoa',$giaban,$namsanxuat,$nhasanxuatid,$loaihanghoaid,'$mota'";
		$i = 1;
		foreach($listtenhinh as $tenhinh)
		{
			if($i == 1)
			{
				$insertcolumns .= " ,hinh";
				$values .= "  ,'$tenhinh'";
				$i++;
				}
			else{
				$insertcolumns .= " ,hinh$i";
				$values .= "  ,'$tenhinh'";
				$i++;
				}
			}
		$insertcolumns .=')';
		$values .= ')';
		$sql = $insertcolumns.$values;
		$hanghoaid = $this->db->ExecuteQueryInsert($sql);
		if($hanghoaid!=0)
		{
			
			//them vao table motathuoctinh
			foreach($listthuoctinh as $thuoctinh){
				$sql = "INSERT INTO motathuoctinh (hanghoaid, thuoctinhid, mota) values($hanghoaid, {$thuoctinh['thuoctinhid']}, '{$thuoctinh['mota']}')";
				$this->db->ExecuteQueryInsert($sql);
			}
			
			
			//dua hinh vao folder HinhAnh
			$i =0;
			foreach($listhinh as $hinh)
			{
				file_put_contents('Resource/HinhAnh/hanghoa/'.$listtenhinh[$i++], base64_decode($hinh['content']));
				}
		
		}
	}
	function suahanghoa($hanghoaid,$mahanghoa, $tenhanghoa, $giaban, $namsanxuat, $nhasanxuatid, $loaihanghoaid, $mota, $listhinh)
	{
		//tạo mảng tên hình ảnh
		$i=1;
	    $prefixtenhinh = str_replace(' ', '', $tenhanghoa).'_';
	    $listtenhinh = array();
	    foreach($listhinh as $hinh)
		{
			if($hinh != '')
			{ 
				$type=str_replace('image/', '', $hinh[0]['type']);
				$filename = $prefixtenhinh.$i++.'.'.$type;
				$listtenhinh[]=$filename;
			}
			else
			{
				$listtenhinh[]='';
				$i++;
				}
		  }
		  
		$sql = "update hanghoa set mahanghoa='$mahanghoa',tenhanghoa= '$tenhanghoa',giaban=$giaban,namsanxuat=$namsanxuat, nhasanxuatid=$nhasanxuatid, loaihanghoaid= $loaihanghoaid, mota = '$mota'";
		$i = 1;
		foreach($listtenhinh as $tenhinh)
		{
			if($tenhinh != '')
			{
				if($i == 1)
				{
					$sql .= " ,hinh = '$tenhinh'";
					$i++;
					}
				else{
					$sql .= " ,hinh$i = '$tenhinh'";
					$i++;
					}
				}
			else
				$i++;
			}
			$sql .=" where hanghoaid = $hanghoaid";
			echo $sql;
		$hanghoaid = $this->db->ExecuteQuery($sql);
			//dua hinh vao folder HinhAnh
			$i =0;
			foreach($listhinh as $hinh)
			{
				if($hinh !='')
				file_put_contents('Resource/HinhAnh/hanghoa/'.$listtenhinh[$i++], base64_decode($hinh[0]['content']));
				else $i++;
				}
		
		}
	}

?>