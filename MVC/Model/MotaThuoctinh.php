<?php
include_once("DataProvider.php");
class MotaThuoctinh
{
	private $db;
	function __construct()
	{
		$this->db = new DataProvider(); 
	}
	
	function SelectMotaThuoctinh($sql)
	{
		return $this->db->FetchAll($sql);
	}
	function getmotathuoctinhbyhanghoaid($hanghoaid)
	{
		$sql = "select motathuoctinhid,mttt.thuoctinhid, tenthuoctinh,mota from thuoctinh tt join motathuoctinh mttt on tt.thuoctinhid = mttt.thuoctinhid where hanghoaid = $hanghoaid";
		
		$tmp = $this->db->FetchAll($sql);
		$listmotathuoctinhbyhanghoaid = array();
		foreach($tmp as $motathuoctinh)
		{
			$motathuoctinh['recid']=$motathuoctinh['motathuoctinhid'];
			$listmotathuoctinhbyhanghoaid[]=$motathuoctinh;
		}
			
		return $listmotathuoctinhbyhanghoaid;
		}
	function themmotathuoctinh($thuoctinhid, $hanghoaid,$motathuoctinh)
	{
		$sql="insert into motathuoctinh(thuoctinhid,hanghoaid,mota) values($thuoctinhid,$hanghoaid, '$motathuoctinh')";

		return $this->db->ExecuteQueryInsert($sql);
	}
	function xoamotathuoctinh($motathuoctinhid)
	{
		$sql="delete from motathuoctinh where motathuoctinhid = $motathuoctinhid";
		return $this->db->ExecuteQuery($sql);
	}
	function xoamotathuoctinhbyhanghoaid($hanghoaid)
	{
		$sql="delete from motathuoctinh where hanghoaid = $hanghoaid";
		return $this->db->ExecuteQuery($sql);
	}
	function suamotathuoctinh($motathuoctinhid, $mota)
	{
		$sql="update motathuoctinh set mota='$mota' where motathuoctinhid=$motathuoctinhid";
		return $this->db->ExecuteQuery($sql);
	}
}
?>