<?php
include_once("DataProvider.php");
class LoaiHanghoa
{
	private $db;
	function __construct()
	{
		$this->db = new DataProvider(); 
	}
	
	function getloaihanghoabyloaihanghoaid($loaihanghoaid){
		return $this->db->Fetch("select * from loaihanghoa where loaihanghoaid = $loaihanghoaid");
		}
	function themloaihanghoa($maloaihanghoa,$tenloaihanghoa, $icon)
	{
		//Them hinh vao folder
		$id = $this->GetCountLoaiHanghoa() + 1;
		$prefixtenhinh = 'LHH'.'_'.$id;
		$type=str_replace('image/', '', $icon['type']);
		$filename = $prefixtenhinh.'.'.$type;
		file_put_contents('Resource/HinhAnh/loaihanghoa/'.$filename, base64_decode($icon['content']));
		
		$sql="insert into loaihanghoa(maloaihanghoa,tenloaihanghoa, icon) values('$maloaihanghoa','$tenloaihanghoa','$filename')";
		return $this->db->ExecuteQueryInsert($sql);
	}
	function xoaloaihanghoa($loaihanghoaid)
	{
		$tmp = $this->db->Fetch("select icon from loaihanghoa where loaihanghoaid=$loaihanghoaid");
		$icon = $tmp['icon'];
		$path = 'Resource/HinhAnh/loaihanghoa/'.$icon;
		if($icon !='')
			if(file_exists($path))
				unlink($path);
		$sql="delete from loaihanghoa where loaihanghoaid = $loaihanghoaid";
		return $this->db->ExecuteQuery($sql);
	}
	function sualoaihanghoa($loaihanghoaid, $maloaihanghoa, $tenloaihanghoa, $icon, $tenicon)
	{
		$sql='';
		if($tenicon != '')
		{
			$sql="update loaihanghoa set maloaihanghoa='$maloaihanghoa',tenloaihanghoa='$tenloaihanghoa' where loaihanghoaid=$loaihanghoaid";
		}
		else
		{
			$prefixtenhinh = 'LHH'.'_'.$loaihanghoaid;
			$type=str_replace('image/', '', $icon['type']);
			$tenicon = $prefixtenhinh.'.'.$type;
			$sql="update loaihanghoa set maloaihanghoa='$maloaihanghoa',tenloaihanghoa='$tenloaihanghoa',icon='$tenicon' where loaihanghoaid=$loaihanghoaid";
		}
		file_put_contents('Resource/HinhAnh/loaihanghoa/'.$tenicon, base64_decode($icon['content']));
		return $this->db->ExecuteQuery($sql);
	}
	function SelectLoaiHanghoa($sql)
	{
		return $this->db->FetchAll($sql);
	}
	function GetCountLoaiHanghoa()
	{
		return $this->db->NumRows("select * from loaihanghoa");
		}
	function GetListLoaiHanghoa()
	{
		return $this->db->FetchAll("select * from loaihanghoa");
		}
}
?>