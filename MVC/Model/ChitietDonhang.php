<?php
include_once("DataProvider.php");
class chitietdonhang
{
	private $db;
	function __construct()
	{
		$this->db = new DataProvider(); 
	}
	
	function Selectchitietdonhang($sql)
	{
		return $this->db->FetchAll($sql);
	}
	function getchitietdonhangbydonhangid($donhangid)
	{
		$sql = "select ctdh.chitietdonhangid,ctdh.soluong, hh.giaban, hh.hanghoaid, hh.hinh,hh.mahanghoa, hh.tenhanghoa,lhh.tenloaihanghoa,nsx.tennhasanxuat  from chitietdonhang ctdh join hanghoa hh on ctdh.hanghoaid = hh.hanghoaid join loaihanghoa lhh on hh.loaihanghoaid = lhh.loaihanghoaid join nhasanxuat nsx on hh.nhasanxuatid = nsx.nhasanxuatid where donhangid = $donhangid";
		
		$tmp = $this->db->FetchAll($sql);
		$listchitietdonhangbydonhangid = array();
		foreach($tmp as $chitietdonhang)
		{
			$chitietdonhang['recid']=$chitietdonhang['chitietdonhangid'];
			$listchitietdonhangbydonhangid[]=$chitietdonhang;
		}
			
		return $listchitietdonhangbydonhangid;
		}
		
		function getchitietdonhangbymadonhang($madonhang)
	{
		$sql = "select ctdh.chitietdonhangid,ctdh.soluong, hh.giaban, hh.hanghoaid, hh.hinh,hh.mahanghoa, hh.tenhanghoa,lhh.tenloaihanghoa,nsx.tennhasanxuat  from chitietdonhang ctdh join hanghoa hh on ctdh.hanghoaid = hh.hanghoaid join loaihanghoa lhh on hh.loaihanghoaid = lhh.loaihanghoaid join nhasanxuat nsx on hh.nhasanxuatid = nsx.nhasanxuatid where madonhang = '$madonhang'";
		
		$tmp = $this->db->FetchAll($sql);
		$listchitietdonhangbydonhangid = array();
		foreach($tmp as $chitietdonhang)
		{
			$chitietdonhang['recid']=$chitietdonhang['chitietdonhangid'];
			$listchitietdonhangbydonhangid[]=$chitietdonhang;
		}
			
		return $listchitietdonhangbydonhangid;
		}
	function themchitietdonhang($donhangid, $listchitietdonhang)
	{
		foreach($listchitietdonhang as $ctdh)
		{
			$sql="insert into chitietdonhang(donhangid,hanghoaid,soluong, giatien) values($donhangid,{$ctdh['hanghoaid']}, {$ctdh['soluong']}, {$ctdh['giaban']})";
			$this->db->ExecuteQueryInsert($sql);
		}
	}
	function xoachitietdonhang($chitietdonhangid)
	{
		$sql="delete from chitietdonhang where chitietdonhangid = $chitietdonhangid";
		return $this->db->ExecuteQuery($sql);
	}
	function xoachitietdonhangbydonhangid($donhangid)
	{
		$sql="delete from chitietdonhang where donhangid = $donhangid";
		return $this->db->ExecuteQuery($sql);
	}
	function suachitietdonhang($chitietdonhangid, $mota)
	{
		$sql="update chitietdonhang set mota='$mota' where chitietdonhangid=$chitietdonhangid";
		return $this->db->ExecuteQuery($sql);
	}
}
?>