<?php
include_once("DataProvider.php");
class DonHang
{
	private $db;
	function __construct()
	{
		$this->db = new DataProvider(); 
	}
	function selectdonhang($sql)
	{
		return $this->db->FetchAll($sql);
	}
	function getlistdonhang()
	{
		$tmp = $this->db->FetchAll("select * from donhang");
		$listdonhang = array();
		foreach($tmp as $donhang)
		{
   			$donhang['recid'] = $donhang['donhangid'];
			$listdonhang[]=$donhang;
		}
		return $listdonhang;
	}
	function getdonhangbydonhangid($donhangid)
	{
		return $this->db->Fetch("select * from donhang where donhangid = $donhangid");
	}
	function getdonhangidbymadonhang($madonhang)
	{
		return $this->db->Fetch("select donhangid from donhang where madonhang = '$madonhang'");
	}
	function getlistdonhangcangiao()
	{
		$tmp = $this->db->FetchAll("select * from donhang where trangthai=2");
		$listdonhang = array();
		foreach($tmp as $donhang)
		{
   			$donhang['recid'] = $donhang['donhangid'];
			$listdonhang[]=$donhang;
		}
		return $listdonhang;
	}
	function getlistdonhangdaxuly()
	{
		$tmp = $this->db->FetchAll("select * from donhang where trangthai=1");
		$listdonhang = array();
		foreach($tmp as $donhang)
		{
   			$donhang['recid'] = $donhang['donhangid'];
			$listdonhang[]=$donhang;
		}
		return $listdonhang;
	}
	function getlistdonhangchuaxuly()
	{
		$tmp = $this->db->FetchAll("select * from donhang where trangthai=0");
		$listdonhang = array();
		foreach($tmp as $donhang)
		{
   			$donhang['recid'] = $donhang['donhangid'];
			$listdonhang[]=$donhang;
		}
		return $listdonhang;
	}
	function themdonhang($tongtien)
	{
		$sqlinsert="insert into donhang(tongtien) values($tongtien)";

		$donhangid =  $this->db->ExecuteQueryInsert($sqlinsert);
		$madonhang = "DH".$donhangid;
		
		$sqlupdate="update donhang set madonhang='$madonhang' where donhangid=$donhangid";
		$this->db->ExecuteQuery($sqlupdate);
		return $donhangid;
	}
	function suadonhang($donhangid, $madonhang)
	{
		$sql="update donhang set madonhang=$madonhang where donhangid=$donhangid";

		return $this->db->ExecuteQuery($sql);
	}
	function xoadonhang($donhangid)
	{
		$sql = "delete from chitietdonhang where donhangid = $donhangid";
		$this->db->ExecuteQuery($sql);
		$sql = "delete from phieugiaohang where donhangid = $donhangid";
		$this->db->ExecuteQuery($sql);
		$sql="delete from donhang where donhangid = $donhangid";
		return $this->db->ExecuteQuery($sql);
	}
	function trangthaidaxacnhan($donhangid)
	{
		$sql="update donhang set trangthai=1 where donhangid=$donhangid";
		return $this->db->ExecuteQuery($sql);
	}
	function trangthaigiaohang($donhangid)
	{
		$sql="update donhang set trangthai=2 where donhangid=$donhangid";
		return $this->db->ExecuteQuery($sql);
	}
	function trangthaidagiaohang($donhangid)
	{
		$sql="update donhang set trangthai=3 where donhangid=$donhangid";
		return $this->db->ExecuteQuery($sql);
	}
}
?>