<?php
include_once("DataProvider.php");
@session_start();
class GioHang
{
	private $db;
	function __construct()
	{
		$this->db = new DataProvider(); 
	}
	public function Them($id, $sl)
	{
		if(isset($_SESSION["GioHang"][$id]))
			$_SESSION["GioHang"][$id] += $sl;
		else
			$_SESSION["GioHang"][$id] = $sl;
		return json_encode($_SESSION["GioHang"]);
	}
	
	public function Xoa($id)
	{
		if(isset($_SESSION["GioHang"][$id]))
			unset($_SESSION["GioHang"][$id]);
	}
	public function XoaAll()
	{
		unset($_SESSION["GioHang"]);
		}
	public static function Sua($id, $sl)
	{
		if(isset($_SESSION["GioHang"][$id]))
			$_SESSION["GioHang"][$id] = $sl;
	}
	
	public function gettongtien()
	{
		$sum = 0;
		foreach($_SESSION['GioHang'] as $hanghoaid => $SoLuong)
		{
			$rs= $this->db->Fetch("SELECT giaban FROM hanghoa WHERE hanghoaid = $hanghoaid");
			$sum += $SoLuong * $rs['giaban'];
		}
		return $sum;
	}
	public function GetSoLuong(){
		return count($_SESSION['GioHang']);
		}
	public function GetListGioHang(){
		if(isset($_SESSION["GioHang"]))
		{
		$i = 0;
		$sql = "SELECT * FROM hanghoa WHERE";
		$result = array();
		foreach($_SESSION["GioHang"] as $hanghoaid => $SoLuong)
		{
			$row =$this->db->Fetch("SELECT * FROM hanghoa WHERE hanghoaid = $hanghoaid");
			$row['soluong'] = $SoLuong;
			$row['recid'] = $hanghoaid;
			$result[] = $row;
		}
		
		return $result;
		}
		else
		return 0;
	}
	public function GetListGioHangw2ui(){
		$i = 0;
		$sql = "SELECT * FROM hanghoa WHERE";
		$result = array();
		$tongtien = 0;
		foreach($_SESSION["GioHang"] as $hanghoaid => $SoLuong)
		{
			$row =$this->db->Fetch("SELECT * FROM hanghoa WHERE hanghoaid = $hanghoaid");
			$row['soluong'] = $SoLuong;
			$row['thanhtien'] = $row['giaban']*$SoLuong;
			$row['recid'] = $hanghoaid;
			$result[] = $row;
			$tongtien += $row['thanhtien'];
		}
		$row = array();
		$row['recid'] = count($result)+1;
		$row['hinh'] = 'null';
		$row['soluong']='<span style="float: right;">Tổng tiền</span>';
		$row['thanhtien'] = $tongtien;
		$row['summary'] = true;
		$result[] = $row;
		return $result;
	}
}
?>