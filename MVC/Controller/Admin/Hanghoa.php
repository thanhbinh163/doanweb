<?php
include_once("./MVC/Model/Hanghoa.php");
include_once("./MVC/Model/NhaSanxuat.php");
include_once("./MVC/Model/LoaiHanghoa.php");
include_once("./MVC/Model/ThuocTinh.php");

$hanghoadb = new Hanghoa();

$nhasanxuatdb = new NhaSanxuat();
$nhasanxuattmp = $nhasanxuatdb->getlistnhasanxuat();
$listnhasanxuat = array();
foreach($nhasanxuattmp as $nhasanxuat)
	$listnhasanxuat[]= array('id'=>$nhasanxuat['nhasanxuatid'],'text'=>$nhasanxuat['tennhasanxuat']);
	
$loaihanghoadb = new LoaiHanghoa();
$loaihanghoatmp = $loaihanghoadb->getlistloaihanghoa();
$listloaihanghoa = array();
foreach($loaihanghoatmp as $loaihanghoa)
	$listloaihanghoa[]= array('id'=>$loaihanghoa['loaihanghoaid'],'text'=>$loaihanghoa['tenloaihanghoa']);

$thuoctinhdb = new ThuocTinh();
$thuoctinhtmp = $thuoctinhdb->getlistthuoctinh();
$listthuoctinh = array();
foreach($thuoctinhtmp as $thuoctinh)
	$listthuoctinh[]= array('id'=>$thuoctinh['thuoctinhid'],'text'=>$thuoctinh['tenthuoctinh']);


include_once("./MVC/View/Admin/Hanghoa.php");
?>