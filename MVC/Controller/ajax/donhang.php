<?php
include_once("./MVC/Model/DonHang.php");
include_once("./MVC/Model/ChitietDonhang.php");
include_once("./MVC/Model/PhieuGiaohang.php");

$donhangdb = new DonHang();
$chitietdonhangdb = new chitietdonhang();
$phieugiaohangdb = new PhieuGiaohang();
if($_REQUEST['cmd']=='get-records')
{
if(isset($_REQUEST['loai']))
{
	if($_REQUEST['loai']=='getdonhangdaxuly')
	{
		$listdonhang = $donhangdb->getlistdonhangdaxuly();
		$result = array();
		$result['total']=count($listdonhang);
		$result['records']=$listdonhang;
		echo json_encode($result,JSON_UNESCAPED_UNICODE);
	}
	else if ($_REQUEST['loai']=='getdonhangcangiao')
	{
		$listdonhang = $donhangdb->getlistdonhangcangiao();
		$result = array();
		$result['total']=count($listdonhang);
		$result['records']=$listdonhang;
		echo json_encode($result,JSON_UNESCAPED_UNICODE);
		}
	}
else
{
	$listdonhang = $donhangdb->getlistdonhang();
$result = array();
$result['total']=count($listdonhang);
$result['records']=$listdonhang;
echo json_encode($result,JSON_UNESCAPED_UNICODE);
	}
}
else if($_REQUEST['cmd']=='save-record')
{
	$data = $_REQUEST['record'];
	if($_REQUEST['loai']=='them')
	$donhangdb->themdonhang($data['tendonhang']);
	else if ($_REQUEST['loai']=='suatrangthai-xacnhan')
	$donhangdb->trangthaidaxacnhan($data['donhangid']);
	else if ($_REQUEST['loai']=='suatrangthai-giaohang')
	$donhangdb->trangthaigiaohang($data['donhangid']);
	else if ($_REQUEST['loai']=='suatrangthai-dagiaohang')
	$donhangdb->trangthaidagiaohang($data['donhangid']);
	else if ($_REQUEST['loai']=='suatrangthai-dagiaohang-list')
	{
		$listdonhangid = $_REQUEST['listdonhangid'];
		foreach($listdonhangid as $donhangid)
		{
			$donhangdb->trangthaidagiaohang($donhangid);
			}
		}
	}
else if($_REQUEST['cmd']=='delete-records')
{
	
	if($donhangdb->xoadonhang($_REQUEST['selected'][0]))
		echo "{'status':'success'}";
	else
	{

		echo "{'status'  : 'error' , 'message' : 'Lỗi khóa ngoại'}";
	
	}
	}
	else if($_REQUEST['cmd']=='delete-record')
{
	$donhang = $_REQUEST['record'];
	$donhangdb->xoadonhang($donhang['donhangid']);
	}
	else if($_REQUEST['cmd']=='getdonhangbymadonhang')
{
	$madonhang = $_REQUEST['madonhang'];
	$donhangid = $donhangdb->getdonhangidbymadonhang($madonhang);
	$donhangid = $donhangid['donhangid'];
	if($donhangid== '')
		{
			$chuoi = <<<EOD
		<div class="row">
  <div style="
	background-color: orange;
    padding-top: 10px;
    padding-bottom: 9px;
    margin-top: 35px;
    margin-right: 10px;
    color: white;
    font-size: large">Không tìm thấy mã đơn <strong id='madon'>{$madonhang}</strong> trong hệ thống</div>
</div>
EOD;
echo $chuoi;
			}
	else
	{
		$donhang = $donhangdb->getdonhangbydonhangid($donhangid);
		$phieugiaohang = $phieugiaohangdb->getphieugiaohangbydonhangid($donhangid);
		$listchitiethoadon = $chitietdonhangdb->getchitietdonhangbydonhangid($donhangid);
		$trangthaidon = '';
		if($donhang['trangthai']==0)
			$trangthaidon='Đơn mới';
		else if($donhang['trangthai']==1)
			$trangthaidon='Đã xác nhận chuyển tiền';
		else if($donhang['trangthai']==2)
			$trangthaidon='Đóng gói, giao hàng';
		else 
			$trangthaidon='Đang giao hàng';
		
		$chuoi = <<<EOD
		<div class="row">
  <div style="
	background-color: orange;
    padding-top: 10px;
    padding-bottom: 9px;
    margin-top: 35px;
    margin-right: 10px;
    color: white;
    font-size: large">Thông tin đơn hàng <strong id='madon'>{$madonhang}</strong></div>
</div>
<div class="row">
  <div id="donhangContainer" style="margin-top: 20px; " class="container">
		<div class="col-xs-6" style="text-align:left; margin-bottom: 30px;">
      <label for="ex2" >Trạng thái đơn hàng </label>
      <button class="btn btn-info">{$trangthaidon}</button>
	  <br>
      <label for="ex2" >Tên người nhận</label>
      <input class="form-control" id="ex2" type="text" value='{$phieugiaohang['tennguoinhan']}' disabled>
      <label for="ex2" >Số điện thoại</label>
      <input class="form-control" id="ex2" type="text" value='{$phieugiaohang['sodienthoai']}' disabled>
      <label for="ex2" >Địa chỉ</label>
      <textarea class="form-control" rows="2" id="diachi" disabled>{$phieugiaohang['diachi']}</textarea>
    </div>
    <div class="row">
      <table class="table table-bordered table-giohang">
        <thead>
          <tr>
            <th style="text-align:center">Hình ảnh</th>
            <th style="text-align:center">Tên hàng hóa</th>
            <th style="text-align:center">Giá bán</th>
            <th style="text-align:center">Số lượng</th>
            <th style="text-align:center">Thành tiền</th>
          </tr>
        </thead>
        <tbody>
          
EOD;
		$tongtien = 0;
		foreach($listchitiethoadon as $cthd)
		{
			$gia = number_format($cthd['giaban']);
			$thanhtien = $cthd['giaban'] * $cthd['soluong'];
			$tongtien += $thanhtien;
			$thanhtienstring = number_format($thanhtien);
		$chuoi .= <<<EOD
		<tr>
            <td style="vertical-align:middle"><img height="15%" src="Resource/HinhAnh/hanghoa/{$cthd['hinh']}"/></td>
            <td class="tenhanghoa" hanghoaid="2" style="vertical-align:middle">{$cthd['tenhanghoa']}</td>
            <td class="giaban" style="vertical-align:middle"">{$gia}</td>
            <td style="vertical-align:middle"><input class="soluong" type="number" value="{$cthd['soluong']}" disabled/></td>
            <td style="vertical-align:middle"><h4 class="thanhtien" style="display: inline-block;">{$thanhtienstring}</h4> vnd</td>
		</tr>
EOD;
		}
		$tongtienstring = number_format($tongtien);
		$chuoi .= <<<EOD
        </tbody>
      </table>
    </div>
    <div class="row">
    <div class="col-xs-4" style="float:right; text-align:left;" ><strong style="font-size:large">Tổng tiền : </strong><span style="font-size:x-large">{$tongtienstring}</span><strong> vnd</strong></div>
    </div>
		</div>
EOD;
echo $chuoi;
		}
	}
else
{}
?>