<?php
include_once("./MVC/Model/DonHang.php");
include_once("./MVC/Model/ChitietDonhang.php");
include_once("./MVC/Model/GioHang.php");
include_once("./MVC/Model/PhieuGiaohang.php");

$giohang = new GioHang();
$donhangdb = new DonHang();
$chitietdonhangdb = new ChitietDonhang();
$phieugiaohangdb = new PhieuGiaohang();
if(isset($_REQUEST['loai']))
{
	if($_REQUEST['loai']=='them')
	{
		echo $giohang->Them($_REQUEST['hanghoaid'],$_REQUEST['soluong']);
	}
	else if($_REQUEST['loai']=='clear')
	{
		echo $giohang->XoaAll();
	}
	else if($_REQUEST['loai']=='xoa')
	{
		echo $giohang->Xoa($_REQUEST['hanghoaid']);
	}
	else if($_REQUEST['loai']=='sua')
	{
		echo $giohang->Sua($_REQUEST['hanghoaid'],$_REQUEST['soluong']);
	}
}
else if (isset($_REQUEST['cmd']))
{
	if($_REQUEST['cmd']=='get-records')
	{
		$listgiohang = $giohang->GetListGioHangw2ui();
		$result = array();
		$result['total']=count($listgiohang);
		$result['records']=$listgiohang;
		echo json_encode($result,JSON_UNESCAPED_UNICODE);
		}
	else if($_REQUEST['cmd']=='save-record')
	{
		$thongtinphieugiao = $_REQUEST['record'];
		$listgiohang = $giohang->GetListGioHang();
		$tongtien = $giohang->gettongtien();
		$donhangid = $donhangdb->themdonhang($tongtien);
		$chitietdonhangdb->themchitietdonhang($donhangid, $listgiohang);
		$phieugiaohangdb->themphieugiaohang($donhangid,$thongtinphieugiao['tennguoinhan'],$thongtinphieugiao['sodienthoai'],$thongtinphieugiao['diachi']);
		$giohang->XoaAll();
		echo "DH".$donhangid;
	}
}

?>