﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vidu02.Domain
{
    public class Sinhvien
    {
        public string Maso { get; set; }
        public string Holot { get; set; }
        public string Ten { get; set; }
        public DateTime? Ngaysinh { get; set; }
        public string NgaysinhString 
        { 
            get
            {
                if (Ngaysinh == null)
                    return "";
                return Ngaysinh.Value.ToString("dd-MM-yyyy");
            }
        }
        public bool? Gioitinh { get; set; }
    }
}
