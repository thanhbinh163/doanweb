﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vidu02.Domain;
using DatabaseLib;

namespace vidu02.Repository
{
    public class DeleteSinhvienRepository : DatabaseLib.Repository
    {
        public string Maso { get; set; }

        public void Execute()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = "DELETE FROM SINHVIEN WHERE Maso = @Maso";
                    cmd.Parameters.Add(new SqlParameter { ParameterName = "@Maso", Value = this.Maso, SqlDbType = System.Data.SqlDbType.NVarChar });

                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
    }
}
