﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vidu02.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }
}