﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using vidu02.Repository;

namespace vidu02.Controllers
{
    public class SinhvienController : Controller
    {
        // GET: Sinhvien
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetListSinhvien(GetListSinhvienRepository cmd)
        {
            var data = cmd.Execute();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InsertSinhvien(vidu02.Domain.Sinhvien model)
        {
            using(var cmd = new InsertSinhvienRepository())
            {
                cmd.item = model;
                cmd.Execute();
            }
            return Json("ok",JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteSinhvien(DeleteSinhvienRepository cmd)
        {
            cmd.Execute();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }
    }
}