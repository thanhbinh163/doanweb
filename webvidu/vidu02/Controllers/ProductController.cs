﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using vidu02.Models;

namespace vidu02.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListProduct()
        {
            var listData = new List<Product>();
            listData.Add(new Product{Id = 1,Code = "sp01",Name = "Nước pepsi", Price = 10000});
            listData.Add(new Product{Id = 2,Code = "sp02",Name = "Nước Coca", Price = 15000});
            listData.Add(new Product { Id = 3, Code = "sp03", Name = "7 Up", Price = 15000 });
            return Json(listData, JsonRequestBehavior.AllowGet);
        }
    }
}