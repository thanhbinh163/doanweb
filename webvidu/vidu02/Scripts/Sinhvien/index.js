﻿var sinhvien = {
    init: function(){
        $("#GetData").click(sinhvien.GetDataClick);
        $('#myGrid').w2grid({
            name: 'myGrid',
            columns: [
                { field: 'Maso', caption: 'Mã số', size: '30%' },
                { field: 'Holot', caption: 'Họ lót', size: '30%' },
                { field: 'Ten', caption: 'Tên', size: '40%' },
                { field: 'NgaysinhString', caption: 'Ngày sinh', size: '120px' },
                { field: 'Gioitinh', caption: 'Giới tính', size: '120px' }
            ]
        });
    },
    GetDataClick: function (e) {
        $.ajax({
            url: "/Sinhvien/GetListSinhvien",
            success: sinhvien.DrawTableWithW2UI
        });
    },
    DrawTableWithW2UI: function(data)
    {
        data = data.map(function (item) {
            return $.extend(true, item, {
                recid: item.Maso
            });
        });
        w2ui.myGrid.clear();
        w2ui.myGrid.add(data);
    },
    DrawTable: function(data)
    {
        //alert(JSON.stringify(data));
        var htmlString = "<table border=1>";
        $.each(data, function (index, item) {
            htmlString += "<tr>";
            {
                htmlString += "<td>" + item.Maso + "</td>";
                htmlString += "<td>" + item.Holot + "</td>";
                htmlString += "<td>" + item.Ten + "</td>";
                htmlString += "<td>" + item.Ngaysinh + "</td>";
                htmlString += "<td>" + item.Gioitinh + "</td>";
            }
            htmlString += "</tr>";
        });
        htmlString += "</table>";
        $("#data-content").html(htmlString);
    }
};
$(document).ready(sinhvien.init);