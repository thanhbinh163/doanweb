﻿var product = {
    init: function(){
        $("#GetData").click(product.GetDataClick);
    },
    GetDataClick: function (e) {
        $.ajax({
            url: "/Product/GetListProduct",
            success:product.DrawTable
        });
    },
    DrawTable: function(data)
    {
        //alert(JSON.stringify(data));
        var htmlString = "<table border=1>";
        $.each(data, function (index, item) {
            htmlString += "<tr>";
            {
                htmlString += "<td>" + item.Id + "</td>";
                htmlString += "<td>" + item.Code + "</td>";
                htmlString += "<td>" + item.Name + "</td>";
                htmlString += "<td>" + item.Price + "</td>";
            }
            htmlString += "</tr>";
        });
        htmlString += "</table>";
        $("#data-content").html(htmlString);
    }
};
$(document).ready(product.init);